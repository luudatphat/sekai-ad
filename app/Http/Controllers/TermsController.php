<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class TermsController extends Controller
{
	public function terms_of_service() {
        return view('home.terms_of_service');
    }
}