<?php

namespace App\Http\Controllers;

use App\Models\Media_File;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    /*
     * Function insert file to DB
     * @input: file request
     * @return: inserted id
     */
    protected function insert_file_to_db($request_file)
    {
        #convert uploaded image to base64
        $content = file_get_contents($request_file->path());
        $base64 = base64_encode($content);
        #insert image to media_files table
        $media_file = new Media_File();
        $media_file->file_name = $request_file->getClientOriginalName();
        $media_file->content = $base64;
        $media_file->file_type = $request_file->getMimeType();
        $media_file->save();
        return $media_file;
    }

    protected function check_permission($model, $key) {
        $object = $model::find($key);
        if (empty($object)) {
            abort(404);
        }
        if ($object->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
    }
}
