<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSendProductRequest;
use App\Models\DeliveryDetail;
use App\Models\ShippingInfo;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Mtb_Genre;
use App\Models\Delivery_Request;
use App\Http\Requests\EditSendProductRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class SendProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = '';
        $per_page = env('PER_PAGES');

        $search = $request->input('search');
        $status = $request->input('status');
        $data['search'] = $search;
        $data['status'] = $status;

        $current_page = $request->input('page') ? $request->input('page') : 0;
        $data['current_page'] = $current_page;
        $column = 'created_at';
        $sort = 'desc';
        if ($request->has(['column', 'sort'])) {
            $column = $request->input('column');
            $sort = $request->input('sort');
        }
        $pagination_url = 'send-product?';

        if ($request->isMethod('post')) {
            $search = $request->input('search');
            $status = $request->input('status');

            $data['search'] = $search;
            $data['status'] = $status;
            if (empty($search) && empty($status)) {

                $query = Delivery_Request::where('supplier_id', Auth::user()->supplier->id);
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
                $products = $query->paginate($per_page);

            } else {

                $query = DB::table('delivery_requests');
                $query = $query->where('supplier_id', Auth::user()->supplier->id);

                if (!empty($status)) {
                    $query->where('delivery_requests.status', '=', $status);
                }

                if (!empty($search)) {
                    $searchprice = $search;
                    $search = '%' . $search . '%';
                    $query = $query->where(function ($query) use ($search, $searchprice) {
                        $query->orWhere('delivery_requests.company_name', 'like', $search)
                            ->orWhere('delivery_requests.first_name', 'like', $search)
                            ->orWhere('delivery_requests.last_name', 'like', $search)
                            ->orWhere('delivery_requests.country', 'like', $search)
                            ->orWhere('delivery_requests.city', 'like', $search)
                            ->orWhere('delivery_requests.address', 'like', $search);
                    });
                }

                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }

                $products = $query->paginate($per_page);
            }

            $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
            $pagination_url .= ($request->input('search') !== '') ? '&search=' . $request->input('search') : null;
            $pagination_url .= ($request->input('status') !== '') ? '&status=' . $request->input('status') : null;
            $products->setPath($pagination_url);
        } else {
            if (empty($search) && empty($status)) {

                $query = Delivery_Request::where('supplier_id', Auth::user()->supplier->id);
                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }
                $products = $query->paginate($per_page);

            } else {

                $query = DB::table('delivery_requests');
                $query = $query->where('supplier_id', Auth::user()->supplier->id);

                if (!empty($status)) {
                    $query->where('delivery_requests.status', '=', $status);
                }

                if (!empty($search)) {
                    $searchprice = $search;
                    $search = '%' . $search . '%';
                    $query = $query->where(function ($query) use ($search, $searchprice) {
                        $query->orWhere('delivery_requests.company_name', 'like', $search)
                            ->orWhere('delivery_requests.first_name', 'like', $search)
                            ->orWhere('delivery_requests.last_name', 'like', $search)
                            ->orWhere('delivery_requests.country', 'like', $search)
                            ->orWhere('delivery_requests.city', 'like', $search)
                            ->orWhere('delivery_requests.address', 'like', $search);
                    });
                }

                if ($column != '' && $sort != '') {
                    $query->orderBy($column, $sort);
                }

                $products = $query->paginate($per_page);
            }

            $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
            $pagination_url .= ($request->input('search') !== '') ? '&search=' . $request->input('search') : null;
            $pagination_url .= ($request->input('status') !== '') ? '&status=' . $request->input('status') : null;
            $products->setPath($pagination_url);
        }
        $data['products'] = $products;

        $list_suppliers_name = array();
        foreach ($products as $product) {

            $name = Supplier::find($product->supplier_id)->company_name_jp;
            $list_suppliers_name[$product->id] = $name;
        }
        $data['list_suppliers_name'] = $list_suppliers_name;
        $data['active'] = 10;
        $data['title'] = '配送依頼画面';
        return view('send_product.list', $data);
    }

    public function create()
    {
        $mtb_genres = Mtb_Genre::all();
        $data['mtb_genres'] = $mtb_genres;
        return view('send_product.create', $data);
    }

    public function create_handler(CreateSendProductRequest $request)
    {
        DB::beginTransaction();
        try
        {
            /*Insert new delivery_request*/
            $delivery_request = new Delivery_Request();
            $delivery_request->supplier_id = Auth::user()->supplier->id;

            $delivery_request->company_name = $request->company_name;
            $delivery_request->last_name = $request->last_name;
            $delivery_request->first_name = $request->first_name;
            $delivery_request->address = $request->address;
            $delivery_request->city = $request->city;
            $delivery_request->country = $request->country;
            $delivery_request->requested_delivery_date = $request->requested_delivery_date;
            $delivery_request->consultation = $request->consultation;

            $delivery_request->status = 2;
            $delivery_request->save();

            /*Insert new delivery details*/
            $arr_detail_delivery = array();
            for($i = 1; $i <= 10; $i++) {
                if ($request->has('product_name_'.$i)) {
                    $delivery_detail = new DeliveryDetail();
                    $delivery_detail->delivery_request_id = $delivery_request->id;
                    $delivery_detail->mtb_genre_id = $request->input('goods_category_'.$i);
                    $delivery_detail->product_name = $request->input('product_name_'.$i);
                    $delivery_detail->item_price = $request->input('price_'.$i);
                    $delivery_detail->number = $request->input('quantity_'.$i);
                    $delivery_detail->save();

                    $genres = Mtb_Genre::find($request->input('goods_category_'.$i));
                    $arr_detail_delivery[$i]['goods_category'] = $genres->content;
                    $arr_detail_delivery[$i]['product_name'] = $request->input('product_name_'.$i);
                    $arr_detail_delivery[$i]['price'] = $request->input('price_'.$i);
                    $arr_detail_delivery[$i]['quantity'] = $request->input('quantity_'.$i);
                    $arr_detail_delivery[$i]['total_revenue'] = $request->input('price_'.$i) * $request->input('quantity_'.$i);
                }
            }
            /*Insert new shipping info*/
            $arr_shipping_info = array();
            for($i = 1; $i <= 10; $i++) {
                if ($request->has('weight_'.$i)) {
                    $shipping_info = new ShippingInfo();
                    $shipping_info->delivery_id = $delivery_request->id;
                    $shipping_info->weight = $request->input('weight_'.$i);
                    $shipping_info->vertical = $request->input('vertical_'.$i);
                    $shipping_info->width = $request->input('width_'.$i);
                    $shipping_info->height = $request->input('height_'.$i);
                    $shipping_info->box_number = $request->input('box_number_'.$i);
                    $shipping_info->save();

                    $arr_shipping_info[$i]['weight'] = $request->input('weight_'.$i);
                    $arr_shipping_info[$i]['vertical'] = $request->input('vertical_'.$i);
                    $arr_shipping_info[$i]['width'] = $request->input('width_'.$i);
                    $arr_shipping_info[$i]['height'] = $request->input('height_'.$i);
                    $arr_shipping_info[$i]['box_number'] = $request->input('box_number_'.$i);
                }
            }

            /*SC-54*/
            $data['company_name_jp']  = Auth::user()->supplier->company_name_jp;
            $data['representative_name_kanji']  = Auth::user()->supplier->representative_name_kanji;
            $data['representative_email_address']  = Auth::user()->supplier->representative_email_address;
            $data['address']  = Auth::user()->supplier->address;
            $data['company_name'] = $request->company_name;
            $data['last_name'] = $request->last_name;
            $data['first_name'] = $request->first_name;
            $data['address'] = $request->address;
            $data['city'] = $request->city;
            $data['country'] = $request->country;
            $data['arr_shipping_info'] = $arr_shipping_info;
            $data['arr_detail_delivery'] = $arr_detail_delivery;
            $data['requested_delivery_date'] = $request->requested_delivery_date;
            $data['consultation'] = $request->consultation;


            Mail::raw($this->notificationCreateSendProduct($data), function($message) use ($data)
            {
                $message->from('support@couxu.jp', Config::get('mail.from.name'));
                $message->subject("【配送依頼】セカイコネクト");
                //$message->to('couxu@benly.co.jp');
                $message->to('support@couxu.jp');
            });
            /*End SC-54*/

            /*SC-55*/
            $data_supplier['email_supplier'] = Auth::user()->email;
            Mail::raw($this->notificationToSupplier($data_supplier), function($message) use ($data_supplier)
            {
                $message->from('support@couxu.jp', Config::get('mail.from.name'));
                $message->subject("【配送依頼 仮申込み完了】セカイコネクトサポート窓口");
                $message->to($data_supplier['email_supplier']);
            });
            /*End SC-55*/

            DB::commit();
            return back()->with('message', trans('messages.update_delivery_success'));
        }
        catch (\Exception $e)
        {
            DB::rollback();
            abort(500);
        }
    }

    public function detail($id)
    {
        //get master table genre
        $mtb_genres = Mtb_Genre::all();
        $data['mtb_genres'] = $mtb_genres;
        $delivery_request = Delivery_Request::find($id);
        /*check product exist*/
        if (empty($delivery_request)) {
            abort(404);
        }
        /*check permission*/
        if ($delivery_request->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
        /*get delivery request data*/
        $data['product'] = $delivery_request;
        /*get delivery details data*/
        $data['delivery_details'] = DeliveryDetail::where('delivery_request_id', $id)->get()->toArray();
        /*get shipping info data*/
        $data['shipping_info'] = ShippingInfo::where('delivery_id', $id)->get()->toArray();

        return view('send_product.detail', $data);
    }

    public function update(EditSendProductRequest $request)
    {
        $id = $request->input('delivery_request_id');
        $delivery_request = Delivery_Request::find($id);
        /*check permission*/
        if (!empty($delivery_request) && $delivery_request->supplier_id == Auth::user()->supplier->id) {
            if ($delivery_request->status != 1) {
                DB::beginTransaction();
                try
                {
                    //Update delivery request
                    $delivery_request->company_name = $request->company_name;
                    $delivery_request->last_name = $request->last_name;
                    $delivery_request->first_name = $request->first_name;
                    $delivery_request->address = $request->address;
                    $delivery_request->city = $request->city;
                    $delivery_request->country = $request->country;
                    $delivery_request->requested_delivery_date = $request->requested_delivery_date;
                    $delivery_request->consultation = $request->consultation;
                    $delivery_request->save();
                    //Update delivery detail
                    #delete old delivery detail
                    DeliveryDetail::where('delivery_request_id', $id)->delete();
                    #insert new delivery detail
                    for($i = 1; $i <= 10; $i++) {
                        if ($request->has('product_name_'.$i)) {
                            $delivery_detail = new DeliveryDetail();
                            $delivery_detail->delivery_request_id = $delivery_request->id;
                            $delivery_detail->mtb_genre_id = $request->input('goods_category_'.$i);
                            $delivery_detail->product_name = $request->input('product_name_'.$i);
                            $delivery_detail->item_price = $request->input('price_'.$i);
                            $delivery_detail->number = $request->input('quantity_'.$i);
                            $delivery_detail->save();
                        }
                    }
                    //Update shipping info
                    #delete old shipping info
                    ShippingInfo::where('delivery_id', $id)->delete();
                    #insert new shipping info
                    for($i = 1; $i <= 10; $i++) {
                        if ($request->has('weight_'.$i)) {
                            $shipping_info = new ShippingInfo();
                            $shipping_info->delivery_id = $delivery_request->id;
                            $shipping_info->weight = $request->input('weight_'.$i);
                            $shipping_info->vertical = $request->input('vertical_'.$i);
                            $shipping_info->width = $request->input('width_'.$i);
                            $shipping_info->height = $request->input('height_'.$i);
                            $shipping_info->box_number = $request->input('box_number_'.$i);
                            $shipping_info->save();
                        }
                    }
                    DB::commit();
                    return back()->with('message', trans('messages.update_delivery_success'));
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    abort(500);
                }
            }
        }
    }

    public function sort(Request $request)
    {
        $data = '';
        $per_page = env('PER_PAGES');

        if ($request->isMethod('post')) {
            $search = $request->input('search');
            $data['search'] = $search;

            $status = $request->input('status');
            $data['status'] = $status;

            $current_page = $request->input('current_page') ? $request->input('current_page') : 0;
            $data['current_page'] = $current_page;
            $column = $request->input('column');
            $sort = $request->input('sort');

            $pagination_url = 'send-product?column=' . $column . '&sort=' . $sort;

            if (empty($search) && empty($status)) {

                $products = Delivery_Request::where('supplier_id', Auth::user()->supplier->id)
                    ->orderBy($column, $sort)
                    ->paginate($per_page);

            } else {
                $query = DB::table('delivery_requests');

                if (!empty($status)) {
                    $query->where('delivery_requests.status', '=', $status);
                }

                if (!empty($search)) {
                    $searchprice = $search;
                    $search = '%' . $search . '%';
                    $query = $query->where(function ($query) use ($search, $searchprice) {
                        $query->orWhere('delivery_requests.company_name', 'like', $search)
                            ->orWhere('delivery_requests.first_name', 'like', $search)
                            ->orWhere('delivery_requests.last_name', 'like', $search)
                            ->orWhere('delivery_requests.country', 'like', $search)
                            ->orWhere('delivery_requests.city', 'like', $search)
                            ->orWhere('delivery_requests.address', 'like', $search);
                    });
                }
                $products = $query->orderBy($column, $sort)
                    ->paginate($per_page);
            }

            $pagination_url .= ($request->input('search') !== '') ? '&search=' . $request->input('search') : null;
            $pagination_url .= ($request->input('status') !== '') ? '&status=' . $request->input('status') : null;
            $products->setPath($pagination_url);

            $data['products'] = $products;
            $content = '';
            foreach ($products as $product) {
                $strStatus = '';
                if ($product->status == 1) {
                    $strStatus = 'OK';
                } else {
                    $strStatus = 'レビュー待ち';
                }
                $content .= '<tr>' .
                    '<td>' . (isset($product->created_at) ? date_format(date_create($product->created_at), 'Y/m/d') : '') . '</td>' .
                    '<td>' . (isset($product->company_name) ? $product->company_name : '') . '</td>' .
                    '<td>' . (isset($product->last_name) ? $product->last_name : '') . '</td>' .
                    '<td>' . (isset($product->first_name) ? $product->first_name : '') . '</td>' .
                    '<td>' . (isset($product->country) ? $product->country : '') . '</td>' .
                    '<td>' . (isset($product->city) ? $product->city : '') . '</td>' .
                    '<td>' . (isset($product->address) ? $product->address : '') . '</td>' .
                    '<td>' . $strStatus . '</td>' .
                    '<td class="text-center" style="vertical-align:middle">' .
                    '<a class="btn btn-primary fix-btn-01" href="/detail-send-product/' . $product->id . '">詳細</a>' .
                    '</td>' .
                    '</tr>';
            }
            if ($content == '') {
                $content = '<tr><td colspan="9">データなし。</td></tr>';
            }
            $data['content'] = $content;
            $data['pagination'] = $products->links()->toHtml();

            return json_encode($data);
        }
    }

    private function notificationCreateSendProduct($data){
        $text = "【企業情報】
・日本企業名：".$data['company_name_jp']."
・担当者名：".$data['representative_name_kanji']."
・メールアドレス：".$data['representative_email_address']."
・住所：".$data['address']."
ーーーーーーーーーーーーーーーー 
【海外配送先情報】
・会社名：".$data['company_name']."
・苗字：".$data['last_name']."
・名前：".$data['first_name']."
・住所：".$data['address']."
・都市：".$data['city']."
・国：".$data['country']."

【配送物情報】";
        foreach ($data['arr_shipping_info'] as $key => $item_ship){
            $text .="
$key.箱当たり
・重量/g：".$item_ship['weight']."
・サイズ縦/cm：".$item_ship['vertical']."
・サイズ横/cm：".$item_ship['width']."
・サイズ高さ/cm：".$item_ship['height']."
・箱 個数: ".$item_ship['box_number']."
";
        }
        $text .="
【配送商品情報】";
        foreach ($data['arr_detail_delivery'] as $key => $item_delivery){
            $text.="
・商品カテゴリ：".$item_delivery['goods_category']."
・商品名：".$item_delivery['product_name']."
・商品単価：".$item_delivery['price']."
・配送個数：".$item_delivery['quantity']."
・合計売上：".$item_delivery['total_revenue']."
";
        }
        $text.="
【備考】
・ご希望納期：".$data['requested_delivery_date']."
・その他：".$data['consultation']."
ーーーーーーーーーーーーーーーー";
        return $text;
    }

    private function notificationToSupplier($data_supplier){
        $text = "配送依頼の仮申込みが完了しました。

配送は、株式会社BENLYと提携したサービスとなります。
下記の内容でBENLY社から配送費用のお見積もりが届きますので、内容をご確認ください。

送信元：couxu@benly.co.jp
メール件名：お見積りのお知らせ@BENLY

メールに承諾して頂くと集荷日程とご請求金額が確定します。
―――――――――――――――――――――――■―――――――――――――――――■■□
COUXU株式会社
サポート窓口
■■■〒101-0042 東京都千代田区神田東松下町31-1 神田三義ビル4F
■■■Tel：03-5298-5190
■■■Fax：03-3525-8972
■■■Mail：support@couxu.jp
■■■HP：http://couxu.jp
■■■Media：http://world-conect.com/
――――――――■――――――――――――――――――――――――――――――――■□■";
        return $text;
    }
}
