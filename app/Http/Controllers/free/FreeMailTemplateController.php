<?php

namespace App\Http\Controllers\free;

use App\Models\Attack;
use App\Models\MailTemplatesAttachments;
use Illuminate\Http\Request;
use App\Models\MailTemplate;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateTemplateMail;
use App\Models\Media_File;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Redirect;

class FreeMailTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/template_mail_list' );
        }
        
        $data = [];
        $data['free_user'] = $_free_user;
        $per_page = env('PER_PAGES');
        $data['template_mail_list'] = MailTemplate::where('supplier_id' , config('constants.FREE_DEFAULT_SUPPLIER_ID'))->paginate($per_page);
//        foreach ($data['template_mail_list'] as $list) {
//            if (!empty($list->attach_file)) {
//                $media_file = Media_File::where('id', $list->attach_file)->get();
//                $list->attach_file = $media_file[0]->file_name;
//            }
//        }
        return view('free.template_mail.list', $data);
    }

    /*
     * Index of create new template mail
     *
     *
     *  */

    public function create_new_template()
    {
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/template_mail_create' );
        }
        $data['free_user'] = $_free_user;
        return view('free.template_mail.create', $data);
    }

    /*
     * Index of create new template mail method post
     *
     *
     *  */
    public function create_new_template_handler(CreateTemplateMail $request)
    {
        /*check attach file mail template*/
        DB::beginTransaction();
        try {
//            $attach_file = "";
//            if (!empty($request->attach_file)) {
//                if ($request->attach_file->isValid()) {
//                    $attach_file_content = file_get_contents($request->attach_file->path());
//                    $attach_file_base64 = base64_encode($attach_file_content);
//                    #insert image to media_files table
//                    $media_file_attach = new Media_File();
//                    $media_file_attach->file_name = $request->attach_file->getClientOriginalName();
//                    $media_file_attach->content = $attach_file_base64;
//                    $media_file_attach->file_type = $request->attach_file->getMimeType();
//                    $media_file_attach->save();
//                    $attach_file = $media_file_attach->id;
//                } else {
//                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
//                }
//            }

            $mail_attach_file = array();
            $files = $request->file('mail_attach_file');

            if ($request->hasFile('mail_attach_file')) {
                foreach ($files as $file) {
                    if ($file->isValid()) {
                        $mail_attach_file[] = $this->insert_file_to_db($file);
                    } else {
                        return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                    }
                }
            }

            /*insert database template mail*/

            $template_new_mail = new MailTemplate();
            $template_new_mail->template_name = $request->template_name;
            $template_new_mail->subject = $request->subject;
            $template_new_mail->supplier_id = config('constants.FREE_DEFAULT_SUPPLIER_ID');
//            if (!empty($attach_file)) {
//                $template_new_mail->attach_file = $attach_file;
//            }
            $template_new_mail->content = $request->contentd;
            $template_new_mail->save();

            if ($mail_attach_file != NULL) {
                foreach ($mail_attach_file as $item) {
                    $mail_attachment = new MailTemplatesAttachments();
                    $mail_attachment->mail_templates_id = $template_new_mail->id;
                    $mail_attachment->media_file_id = $item->id;
                    $mail_attachment->type = config('constants.TYPE_ATTACHMENT');
                    $mail_attachment->save();
                }
            }

            DB::commit();
            return redirect('/template_mail_create')->with('message', trans('messages.create_new_template_mail_success'));
        } catch (\Exception $e) {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }

        /*end insert database template mail*/
    }

    /*
     * Detail Template mail
     *
     *
     *  */
    public function edit_template_mail($id)
    {
        // $this->check_permission(new MailTemplate(), $id);
        $data = [];
        $_free_user = Auth::user()->free_user;
        if ( $_free_user != '1' ) {
            return Redirect::to( '/template_mail_detail/' . $id );
        }
        $data['free_user'] = $_free_user;
        if ($id) {
            $data['template_mail_list'] = MailTemplate::where('id', $id)->get();

        }
        return view('free.template_mail.detail', $data);
    }

    /*
     * Update Template Mail
     *
     *
     *  */
    public function update_template_mail(CreateTemplateMail $request)
    {
        
        // $this->check_permission(new MailTemplate(), $request->id);
        DB::beginTransaction();
        try {

//            $attach_file = "";
//            if (!empty($request->attach_file)) {
//                if ($request->attach_file->isValid()) {
//                    /*update media file*/
//                    if (!empty($request->post_attach)) {
//                        $attach_file_content = file_get_contents($request->attach_file->path());
//                        $attach_file_base64 = base64_encode($attach_file_content);
//                        #insert image to media_files table
//                        $media_file_attach = Media_File::find($request->post_attach);
//                        $media_file_attach->file_name = $request->attach_file->getClientOriginalName();
//                        $media_file_attach->content = $attach_file_base64;
//                        $media_file_attach->file_type = $request->attach_file->getMimeType();
//                        $media_file_attach->save();
//                        $attach_file = $media_file_attach->id;
//                    } else {
//                        $attach_file_content = file_get_contents($request->attach_file->path());
//                        $attach_file_base64 = base64_encode($attach_file_content);
//                        #insert image to media_files table
//                        $media_file_attach = new Media_File();
//                        $media_file_attach->file_name = $request->attach_file->getClientOriginalName();
//                        $media_file_attach->content = $attach_file_base64;
//                        $media_file_attach->file_type = $request->attach_file->getMimeType();
//                        $media_file_attach->save();
//                        $attach_file = $media_file_attach->id;
//                    }
//
//
//                } else {
//                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
//                }
//            }

            $mail_attach_file = array();
            $files = $request->file('mail_attach_file');

            if ($request->hasFile('mail_attach_file')) {
                foreach ($files as $file) {
                    if ($file->isValid()) {
                        $mail_attach_file[] = $this->insert_file_to_db($file);
                    } else {
                        return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                    }
                }
            }



            /*insert database template mail*/

            $template_new_mail = MailTemplate::find($request->id);
            $template_new_mail->template_name = $request->template_name;
            $template_new_mail->subject = $request->subject;
//            if (!empty($attach_file)) {
//                $template_new_mail->attach_file = $attach_file;
//            }
            $template_new_mail->content = $request->contentd;
            $template_new_mail->save();

//            $files_delete_input = $request->input('array-delete-media');
//            $files_delete_array = array();
//            if($files_delete_input != ""){
//                $files_delete_array = explode(",",$files_delete_input);
//            }
//            if(is_array($files_delete_array) && count($files_delete_array) > 0){
//                foreach ($files_delete_array as $item_delete){
//                    DB::table('mail_templates_attachments')->where('media_file_id', '=', $item_delete)->where('mail_templates_id','=',$request->id)->delete();
//                }
//            }


            if ($mail_attach_file != NULL) {
                if ($request->hasFile('mail_attach_file')) {
                    DB::table('mail_templates_attachments')->where('mail_templates_id','=',$request->id)->delete();
                }
                foreach ($mail_attach_file as $item) {
                    $mail_attachment = new MailTemplatesAttachments();
                    $mail_attachment->mail_templates_id = $template_new_mail->id;
                    $mail_attachment->media_file_id = $item->id;
                    $mail_attachment->type = config('constants.TYPE_ATTACHMENT');
                    $mail_attachment->save();
                }
            }

            DB::commit();
            return redirect("/free/template_mail_detail/{$request->id}")->with('message', trans('messages.update_template_mail_success'));
        } catch (\Exception $e) {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }
    }

    /*
     * Delete Template Mail
     *
     *
     *  */
    public function delete_template_mail($id)
    {
        $this->check_permission(new MailTemplate(), $id);
        if ($id) {
            $template_email = MailTemplate::find($id);
            if(!empty($template_email->attach_file)){
                $media_file = Media_File::find($template_email->attach_file);
                $media_file->delete();
            }

            if(isset($template_email->mail_attachmennt) && count($template_email->mail_attachmennt) > 0){
                foreach ($template_email->mail_attachmennt as $item) {
                    $media_file = Media_File::find($item->media_files->id);
                    $media_file->delete();
                }
            }

            $template_email->delete();
        }
        return redirect('/free/template_mail_list')->with('message', trans('messages.delete_template_mail_success'));
    }

    public function load_mail_template(Request $request)
    {
        $attack_id = $request->attack_id;
        $attack = Attack::find($attack_id);
        if (empty($attack)) {
            abort(404);
        }
        /*check permission*/
        if ($attack->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
        $buyer = $attack->request->buyer;

        $template_mail_id = $request->template_mail_id;
        $mail_template = MailTemplate::find($template_mail_id);
        if ($mail_template->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }

        $list_mail_attachment = $mail_template->mail_attachmennt;
        $array_attach = array();
        foreach ($list_mail_attachment as $item){
            $array_attach[] = $item->media_files->file_name;
        }

        $template_mail = $mail_template->toArray();
        $search = array('*Company-Name*','*Buyer-Name*');
        $replace = array($buyer->company_name, $buyer->mtb_title_id . '. ' . $buyer->name . ' ' . $buyer->surname);
        $template_mail['subject'] = str_replace($search, $replace,  $template_mail['subject']);
        $template_mail['content'] = str_replace($search, $replace,  $template_mail['content']);

        return json_encode(array('template_mail' => $template_mail ,'list_attach' => $array_attach));
    }
}
