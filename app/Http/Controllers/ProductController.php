<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Media_File;
use App\Models\Mtb_Date_Range;
use App\Models\Mtb_Request_To_Buyer;
use App\Models\Mtb_Sale_Channel;
use App\Models\Product;
use App\Models\Product_Date_Range;
use App\Models\Product_Request_To_Buyer;
use App\Models\Product_Sale_Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function product_list(Request $request)
    {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == 1 ) {
            return Redirect::to( '/free/home');
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        /*Get user product list*/
        $per_page = env('PER_PAGES');

        $current_page = $request->input('page') ? $request->input('page') : 0;
        $data['current_page'] = $current_page;
        $column = 'created_at';
        $sort = 'desc';
        if ($request->has(['column', 'sort'])) {
            $column   = $request->input('column');
            $sort     = $request->input('sort');
        }
        $pagination_url = 'product_list?';
        $search = trim($request->input('search'));
        $data['search'] = $search;
        if (empty($search)) {
            $query = Product::where('supplier_id', Auth::user()->supplier->id);
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
            $products = $query->paginate($per_page);
            /*get image show product list*/
           foreach ($products as $row){
                $media_file = Media_File::where('id' , $row['product_image_main'])->get();
                $row['image'] = $media_file[0]['content'];
           }
            /*end get image show product list*/

        } else {
            $search_value = $search;
            $search_string = '%'.$search.'%';
            $query = Product::where('supplier_id', Auth::user()->supplier->id)
            ->where(function ($query) use ($search_string, $search_value) {
                $query->orWhere('created_at', 'like', $search_string)
                    ->orWhere('example_price', '=', $search_value)
                    ->orWhere('product_name', 'like', $search_string)
                    ->orWhere('product_description','like', $search_string);
            });
            if ($column != '' && $sort != '') {
                $query->orderBy($column, $sort);
            }
            $products = $query->paginate($per_page);
        }
        $pagination_url .= ($column !== '' && $sort !== '') ? '&column=' . $column . '&sort=' . $sort : null;
        $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
        $products->setPath($pagination_url);
        $data['products'] = $products;
        return view('product.list', $data);
    }

    public function product_detail($id)
    {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == 1 ) {
            return Redirect::to( '/free/home');
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $product = Product::find($id);
        if (empty($product)) {
            abort(404);
        }
        if ($product->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
        $data['product']  = $product;
        return view('product.detail', $data);
    }

    public function product_edit($id)
    {
        $product = Product::find($id);
        if (empty($product)) {
            abort(404);
        }
        if ($product->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
        $data['product']  = $product;
        $data['mtb_date_ranges'] = Mtb_Date_Range::all();
        $data['mtb_sale_channels'] = Mtb_Sale_Channel::all();
        $data['mtb_request_to_buyers'] = Mtb_Request_To_Buyer::all();
        $data['list_end_user_date_range'] = $product->mtb_date_ranges;
        $data['list_sale_channels'] = $product->mtb_sale_channels;
        $data['list_request_to_buyer'] = $product->mtb_request_to_buyers;
        return view('product.edit', $data);
    }

    public  function product_update(UpdateProductRequest $request)
    {
        DB::beginTransaction();
        try
        {
            $id = $request->input('id');
            $new_product = Product::find($id);

            /*Upload image to DB*/
            #main picture
            if ($request->hasFile('picture_main')) {
                if ($request->picture_main->isValid()) {
                    $picture_main = $this->insert_file_to_db($request->picture_main);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            #picture 2
            if ($request->hasFile('picture_2')) {
                if ($request->picture_2->isValid()) {
                    $picture_2 = $this->insert_file_to_db($request->picture_2);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            #picture 3
            if ($request->hasFile('picture_3')) {
                if ($request->picture_3->isValid()) {
                    $picture_3 = $this->insert_file_to_db($request->picture_3);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            #picture 4
            if ($request->hasFile('picture_4')) {
                if ($request->picture_4->isValid()) {
                    $picture_4 = $this->insert_file_to_db($request->picture_4);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            /*Insert new product to DB*/
            if(count($request->end_customer_gender) == 2)
            {
                $end_customer_gender = config('constants.GENDER_BOTH');
            }
            else
            {
                $end_customer_gender = $request->input('end_customer_gender.0');
            }

            $new_product->supplier_id = Auth::user()->supplier->id;
            $new_product->flag_send_example_request = $request->flag_send_example;
            $new_product->product_name = $request->product_name;
            $new_product->product_image_main = isset($picture_main) ? $picture_main->id : $new_product->product_image_main;
            $new_product->product_image_2 = isset($picture_2) ? $picture_2->id : $new_product->product_image_2;
            $new_product->product_image_3 = isset($picture_3) ? $picture_3->id : $new_product->product_image_3;
            $new_product->product_image_4 = isset($picture_4) ? $picture_4->id : $new_product->product_image_4;
            $new_product->product_store_video_url = $request->product_store_video_url;
            $new_product->product_description_url = $request->product_description_url;
            $new_product->product_description = $request->product_description;
            $new_product->fob_price = $request->fob_price;
            $new_product->moq = $request->moq;
            $new_product->length = $request->length;
            $new_product->width = $request->width;
            $new_product->height = $request->height;
            $new_product->packing_weight = ($request->packing_weight != '') ? $request->packing_weight : NULL;
            $new_product->sale_date = $request->sale_date;
            $new_product->sale_area = $request->sale_area;
            $new_product->deploy_ng_country = $request->deploy_ng_country;
            $new_product->example_price = $request->example_price;
            $new_product->different_point = $request->different_point;
            $new_product->end_customer_gender = $end_customer_gender;
            //$new_product->status = $new_product->status;
            $new_product->save();

            DB::table('product_date_ranges')->where('product_id', '=', $id)->delete();
            #insert product date ranges to DB
            foreach ($request->end_user_date_range as $date_range_id) {
                $product_date_range = new Product_Date_Range();
                $product_date_range->product_id = $new_product->id;
                $product_date_range->mtb_date_range_id = $date_range_id;
                $product_date_range->save();
            }

            DB::table('product_request_to_buyers')->where('product_id', '=', $id)->delete();

            #insert product request to buyer to DB
            foreach ($request->request_to_buyer as $request_to_buyer_id) {
                $request_to_buyer = new Product_Request_To_Buyer();
                $request_to_buyer->product_id = $new_product->id;
                $request_to_buyer->mtb_request_to_buyer_id = $request_to_buyer_id;
                $request_to_buyer->save();
            }

            DB::table('product_sale_channels')->where('product_id', '=', $id)->delete();

            #insert product sale channel to DB
            foreach ($request->sale_channels as $sale_channel_id) {
                $sale_channel = new Product_Sale_Channel();
                $sale_channel->product_id = $new_product->id;
                $sale_channel->mtb_sale_channel_id = $sale_channel_id;
                $sale_channel->save();
            }
            DB::commit();

            return redirect('/product_detail/'.$id)->with('message',trans('messages.create_product_success'));
        }
        catch (\Exception $e)
        {
            DB::rollback();
            //abort(500, 'Unauthorized action.');
            echo $e;
        }
    }

    public function create_product()
    {
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == 1 ) {
            return Redirect::to( '/free/home');
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $data['mtb_date_ranges'] = Mtb_Date_Range::all();
        $data['mtb_sale_channels'] = Mtb_Sale_Channel::all();
        $data['mtb_request_to_buyers'] = Mtb_Request_To_Buyer::all();
        return view('product.create',$data);
    }

    public function create_product_handler(CreateProductRequest $request)
    {
        DB::beginTransaction();
        try
        {
            /*Upload image to DB*/
            #main picture
            if ($request->hasFile('picture_main')) {
                if ($request->picture_main->isValid()) {
                    $picture_main = $this->insert_file_to_db($request->picture_main);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            #picture 2
            if ($request->hasFile('picture_2')) {
                if ($request->picture_2->isValid()) {
                    $picture_2 = $this->insert_file_to_db($request->picture_2);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            #picture 3
            if ($request->hasFile('picture_3')) {
                if ($request->picture_3->isValid()) {
                    $picture_3 = $this->insert_file_to_db($request->picture_3);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            #picture 4
            if ($request->hasFile('picture_4')) {
                if ($request->picture_4->isValid()) {
                    $picture_4 = $this->insert_file_to_db($request->picture_4);
                } else {
                    return view('errors.display', ['error_message' => trans('messages.file_upload_error')]);
                }
            }
            /*Insert new product to DB*/
            if(count($request->end_customer_gender) == 2)
            {
                $end_customer_gender = config('constants.GENDER_BOTH');
            }
            else
            {
                $end_customer_gender = $request->input('end_customer_gender.0');
            }
            $new_product = new Product();
            $new_product->supplier_id = Auth::user()->supplier->id;
            $new_product->flag_send_example_request = $request->flag_send_example;
            $new_product->product_name = $request->product_name;
            $new_product->product_image_main = isset($picture_main) ? $picture_main->id : NULL;
            $new_product->product_image_2 = isset($picture_2) ? $picture_2->id : NULL;
            $new_product->product_image_3 = isset($picture_3) ? $picture_3->id : NULL;
            $new_product->product_image_4 = isset($picture_4) ? $picture_4->id : NULL;
            $new_product->product_store_video_url = $request->product_store_video_url;
            $new_product->product_description_url = $request->product_description_url;
            $new_product->product_description = $request->product_description;
            $new_product->fob_price = $request->fob_price;
            $new_product->moq = $request->moq;
            $new_product->length = $request->length;
            $new_product->width = $request->width;
            $new_product->height = $request->height;
            $new_product->packing_weight = ($request->packing_weight != '') ? $request->packing_weight : NULL;
            $new_product->sale_date = $request->sale_date;
            $new_product->sale_area = $request->sale_area;
            $new_product->deploy_ng_country = $request->deploy_ng_country;
            $new_product->example_price = $request->example_price;
            $new_product->different_point = $request->different_point;
            $new_product->end_customer_gender = $end_customer_gender;
            $new_product->status = config('constants.STATUS_PRODUCT_WAITING');
            $new_product->save();
            #insert product date ranges to DB
            foreach ($request->end_user_date_range as $date_range_id) {
                $product_date_range = new Product_Date_Range();
                $product_date_range->product_id = $new_product->id;
                $product_date_range->mtb_date_range_id = $date_range_id;
                $product_date_range->save();
            }
            #insert product request to buyer to DB
            foreach ($request->request_to_buyer as $request_to_buyer_id) {
                $request_to_buyer = new Product_Request_To_Buyer();
                $request_to_buyer->product_id = $new_product->id;
                $request_to_buyer->mtb_request_to_buyer_id = $request_to_buyer_id;
                $request_to_buyer->save();
            }
            #insert product sale channel to DB
            foreach ($request->sale_channels as $sale_channel_id) {
                $sale_channel = new Product_Sale_Channel();
                $sale_channel->product_id = $new_product->id;
                $sale_channel->mtb_sale_channel_id = $sale_channel_id;
                $sale_channel->save();
            }
            DB::commit();
            return redirect('/create_product')->with('message',trans('messages.create_product_success'));
        }
        catch (\Exception $e)
        {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }
    }

    public function sort(Request $request) {
        $data = '';
        $per_page = env('PER_PAGES');

        if($request->isMethod('post')){
            $search = $request->input('search');
            $data['search'] = $search;

            $current_page = $request->input('current_page') ? $request->input('current_page') : 0;
            $data['current_page'] = $current_page;

            $column = 'created_at';
            $sort = 'desc';
            if ($request->has(['column', 'sort'])) {
                $column   = $request->input('column');
                $sort     = $request->input('sort');
            }

            $pagination_url = 'product_list?column=' . $column . '&sort=' . $sort;

            if(empty($search)){
                $products = Product::where('supplier_id', Auth::user()->supplier->id)
                    ->orderBy($column,$sort)
                    ->paginate($per_page);
                //$products->setPath($pagination_url);

            }else{
                $query = Product::where('supplier_id', Auth::user()->supplier->id);

                $search_value = $search;
                $search_string = '%'.$search.'%';
                $query = Product::where('supplier_id', Auth::user()->supplier->id)
                    ->where(function ($query) use ($search_string, $search_value) {
                        $query->orWhere('created_at', 'like', $search_string)
                            ->orWhere('example_price', '=', $search_value)
                            ->orWhere('product_name', 'like', $search_string)
                            ->orWhere('product_description','like', $search_string);
                    });
                $products = $query->orderBy($column,$sort)
                    ->paginate($per_page);
            }

            $pagination_url .= ($request->input('search') !== '' && !empty($request->input('search'))) ? '&search=' . $request->input('search') : null;
            $products->setPath($pagination_url);

            $data['products'] = $products;
            $content = '';
            foreach ($products as $product)
            {
                $content.= '<tr>'.
                    //'<td>'.$product->id.'</td>'.
                    '<td>'.date_format(date_create($product->created_at),'Y-m-d').'</td>'.
                    '<td>'.$product->product_name.'</td>'.
                    '<td width="200px;" class="td-price-list"><img crossOrigin="Anonymous"
                             src="'.url('/image/'.$product->product_image_main).'"
                             width="100%"/>
                    </td>'.
                    '<td><div id="overflow">'.nl2br(htmlspecialchars($product->product_description)).'</div></td>'.
                    '<td>'.$product->example_price.'</td>'.
                    '<td class="text-center" style="vertical-align: middle">'.
                    '<a class="btn btn-primary fix-btn-01" href="/product_detail/'.$product->id.'">詳細</a>'.
                    '</td>'.
                    '</tr>';
            }
            if ($content == '') {
                $content = '<tr><td colspan="5">データなし。</td></tr>';
            }
            $data['content'] = $content;
            $data['pagination'] = $products->links()->toHtml();

            return json_encode( $data );
        }
    }
}
