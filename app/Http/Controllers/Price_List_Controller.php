<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel as Excel;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Media_File;
use Illuminate\Support\Facades\Auth;
use App\Models\Price_List;
use App\Models\Price_List_Detail;
use Illuminate\Support\Facades\DB;
use PHPExcel_Worksheet_MemoryDrawing;
use Response;
use Redirect;

class Price_List_Controller extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    /**
     *
     *index price list
     *
     */
    public function index()
    {
        $data = [];
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $price_list = Price_List::where('supplier_id', Auth::user()->supplier->id)->orderBy('id','desc')->paginate(env('PER_PAGES'));
        $data['price_list'] = $price_list;
        return view('price_list.list', $data);
    }

    /**
     *
     *index create list detail price
     *
     */
    public function create_new_price_list_detail()
    {
        $data = [];
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        return view('price_list.create', $data);
    }

    /**
     *
     *get product list chose insert price list detail
     *
     */
    public function get_product_list(Request $request)
    {
        $data = [];
        $id_list = $request->id;
        $array_id = [];
        foreach (explode(",", $id_list) as $value) {
            if (!empty($value)) {
                $array_id[] = $value;
            }
        }
        $search = $request->search_string;
        $search_value = $search;
        $search_string = '%' . $search . '%';
        $data['result'] = Product::where('supplier_id', Auth::user()->supplier->id)
            ->where(function ($query) use ($search_string, $search_value) {
                $query->orWhere('created_at', 'like', $search_string)
                    ->orWhere('example_price', '=', $search_value)
                    ->orWhere('product_name', 'like', $search_string)
                    ->orWhere('product_description', 'like', $search_string);
            })->whereNotIn('id', $array_id)->get();
        if(count($data['result']) > 0){
            foreach ($data['result'] as $value) {
                $media_file = Media_File::where('id', $value->product_image_main)->get();
                $value->product_image_main = $media_file[0]->content;
            }
            $data['msg'] = "";
        }
        else{
            $data['msg'] = "この価格表にすでにすべての商品を入れています。新たに商品を登録してから、価格表に再度追加してください。";
        }

        return Response::json($data);
    }

    /**
     *
     *Save price list detail
     *
     */
    public function create_new_price_list_detail_handler(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = [];
            if (!empty($request->id_price_list)) {
                $update_price_list = Price_List::find($request->id_price_list);
                $update_price_list->price_list_name = $request->price_list_name;
                $update_price_list->save();
                $id_price_list = $update_price_list->id;
            } else {
                $new_price_list = new Price_List();
                $supplier_id = Auth::user()->supplier->id;
                $new_price_list->supplier_id = $supplier_id;
                $new_price_list->price_list_name = $request->price_list_name;
                $new_price_list->save();
                $id_price_list = $new_price_list->id;
            }
            $data['result']['price_list_id'] = $id_price_list;
            $detail_price_list = $request->price_list_detail;
            if (!empty($detail_price_list['price_list_detail_id'])) {
                $update_price_list_detail = Price_List_Detail::find($detail_price_list['price_list_detail_id']);
                $update_price_list_detail->name_product_en = $detail_price_list['name_product_en'];
                $update_price_list_detail->price_list_fob_price = $detail_price_list['price_list_fob_price'];
                $update_price_list_detail->price_list_moq = $detail_price_list['price_list_moq'];
                $update_price_list_detail->price_list_length = $detail_price_list['price_list_length'];
                $update_price_list_detail->price_list_width = $detail_price_list['price_list_width'];
                $update_price_list_detail->price_list_height = $detail_price_list['price_list_height'];
                $update_price_list_detail->price_list_input = $detail_price_list['price_list_input'];
                $update_price_list_detail->price_list_packing_weight = $detail_price_list['price_list_packing_weight'];

                $update_price_list_detail->save();
                $id_price_list_detail = $update_price_list_detail->id;
                $msg = "保存に成功しました。";
            } else {
                $new_price_list_detail = new Price_List_Detail();
                $new_price_list_detail->price_list_id = $id_price_list;
                $new_price_list_detail->product_id = $detail_price_list['product_id'];
                $new_price_list_detail->name_product_en = $detail_price_list['name_product_en'];
                $new_price_list_detail->price_list_fob_price = $detail_price_list['price_list_fob_price'];
                $new_price_list_detail->price_list_moq = $detail_price_list['price_list_moq'];
                $new_price_list_detail->price_list_length = $detail_price_list['price_list_length'];
                $new_price_list_detail->price_list_width = $detail_price_list['price_list_width'];
                $new_price_list_detail->price_list_height = $detail_price_list['price_list_height'];
                $new_price_list_detail->price_list_input = $detail_price_list['price_list_input'];
                $new_price_list_detail->price_list_packing_weight = $detail_price_list['price_list_packing_weight'];
                $new_price_list_detail->save();
                $id_price_list_detail = $new_price_list_detail->id;
                $msg = "保存に成功しました。";
            }
            $data['result']['price_list_detail_id'] = $id_price_list_detail;
            $data['result']['msg'] = $msg;
            DB::commit();
            return Response::json($data);
        } catch (\Exception $e) {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }
    }

    /**
     *
     *Delete price list detail
     *
     */
    public function delete_price_list_detail(Request $request)
    {
        $this->check_permission(new Price_List(), $request->price_list_id);
        $data = [];
        if ($request->length_table == 1 && $request->price_list_id) {
            $delete_price_list = Price_List::find($request->price_list_id);
            if (count($delete_price_list) > 0) {
                $delete_price_list->delete();
            }
            DB::table('price_list_detail')->where('price_list_id', $request->price_list_id)->delete();
            $data['result']['msg'] = "保存に成功しました。";
        } else {
            if ($request->id) {
                DB::table('price_list_detail')->where('id', $request->id)->delete();
                $data['result']['msg'] = "保存に成功しました。";
            }
        }
        return Response::json($data);
    }

    /**
     *
     *Delete price list
     *
     */
    public function delete_price_list($id)
    {
        $this->check_permission(new Price_List(), $id );
        /*delete price list*/
        DB::table('price_list_detail')->where('price_list_id', $id)->delete();
        $delete_price_list = Price_List::find($id);
        $delete_price_list->delete();
        /*end delete price list*/
        return redirect('/price_list')->with('message', trans('messages.price_list_delete_success'));
    }

    /**
     *
     *Edit price list
     *
     */
    public function edit_price_list_detail($id)
    {
        $data = [];
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/home' );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */

        $this->check_permission(new Price_List(), $id );
        $price_list = Price_List::where('id', $id)->get();
        $price_list_detail = Price_List_Detail::where('price_list_id', $id)->get();
        foreach ($price_list_detail as $list) {
            $product_detail = Product::where('id', $list['product_id'])->get();
            $list['image'] = $product_detail[0]->product_image_main;
        }
        foreach ($price_list_detail as $list) {
            $media_file = Media_File::where('id', $list['image'])->get();
            $list['image'] = $media_file[0]->content;
        }
        $data['price_list'] = $price_list;
        $data['price_list_detail'] = $price_list_detail;
        return view('price_list.detail', $data);
    }

    /**
     *
     * DownLoad PDF Call
     *
     */
    public function download_file_pdf($id)
    {
        $this->check_permission(new Price_List(), $id);
        $data = [];
        $data["price_list"] = Price_List::where('id', $id)->get();
        $price_list_detail = Price_List_Detail::where('price_list_id', $id)->get();
//        foreach ($price_list_detail as $list) {
//            $product_detail = Product::where('id', $list['product_id'])->get();
//            $list['image'] = $product_detail[0]->product_image_main;
//        }
//        foreach ($price_list_detail as $list) {
//            $media_file = Media_File::where('id', $list['image'])->get();
//            $list['image'] = $media_file[0]->content;
//        }
        $data['price_list_detail'] = $price_list_detail;
        $pdf = PDF::loadView('price_list.pricelisttemplatepdf', $data);
        $name = $data["price_list"][0]['price_list_name'].date('Y-m-d h:i:s').".pdf";
        return $pdf->download($name);
        //return view('price_list.pricelisttemplatepdf', $data);
    }
    /**
     *
     * DownLoad Excel Call
     *
     */
    public function download_file_excel($id)
    {
        $this->check_permission(new Price_List(), $id);
        $data = [];
        $data["price_list"] = Price_List::where('id', $id)->get();
        $price_list_detail = Price_List_Detail::where('price_list_id', $id)->get();
        foreach ($price_list_detail as $list) {
            $product_detail = Product::where('id', $list['product_id'])->get();
            $list['image'] = $product_detail[0]->product_image_main;
        }
        foreach ($price_list_detail as $list) {
            $media_file = Media_File::where('id', $list['image'])->get();
            $list['image'] = $media_file[0]->content;
        }
        $data['price_list_detail'] = $price_list_detail;
        $name = $data["price_list"][0]['price_list_name'].date('Y-m-d h:i:s');
        Excel::create($name, function ($excel) use ($data) {
            $excel->sheet('New sheet', function ($sheet) use ($data) {
                $sheet->loadView('price_list.pricelisttemplateexcel', $data);
                foreach ($data['price_list_detail'] as $key => $list) {
                    $current_row = 7;
                    $i = base64_decode($list['image']);
                    $im = imagecreatefromstring($i);
                    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
                    $objDrawing->setImageResource($im);
                    $objDrawing->setHeight(100);
                    $objDrawing->setCoordinates('A' . ($current_row + $key));
                    $objDrawing->setWorksheet($sheet);
                }
            });
        })->export('xls');
    }
    /**
     *
     * Save all Price List different
     *
     *
     */
    public function save_diff_name(Request $request){
        $this->check_permission(new Price_List(), $request->id_price_list);
        DB::beginTransaction();
        $data = [];
        try {
            $new_price_list = new Price_List();
            $supplier_id = Auth::user()->supplier->id;
            $new_price_list->supplier_id = $supplier_id;
            $new_price_list->price_list_name = $request->new_price_list_name;
            $new_price_list->save();
            $id_price_list = $new_price_list->id;
            $data['id_price_list'] = $id_price_list;
            $data_price_list_detail = Price_List_Detail::where('price_list_id' ,$request->id_price_list)->get();
            foreach ($data_price_list_detail as $detail_price_list){
                $new_price_list_detail = new Price_List_Detail();
                $new_price_list_detail->price_list_id = $id_price_list;
                $new_price_list_detail->product_id = $detail_price_list['product_id'];
                $new_price_list_detail->name_product_en = $detail_price_list['name_product_en'];
                $new_price_list_detail->price_list_fob_price = $detail_price_list['price_list_fob_price'];
                $new_price_list_detail->price_list_moq = $detail_price_list['price_list_moq'];
                $new_price_list_detail->price_list_length = $detail_price_list['price_list_length'];
                $new_price_list_detail->price_list_width = $detail_price_list['price_list_width'];
                $new_price_list_detail->price_list_height = $detail_price_list['price_list_height'];
                $new_price_list_detail->price_list_input = $detail_price_list['price_list_input'];
                $new_price_list_detail->price_list_packing_weight = $detail_price_list['price_list_packing_weight'];
                $new_price_list_detail->save();
            }
            DB::commit();
            return Response::json($data);
        }
        catch (\Exception $e) {
            DB::rollback();
            abort(500, 'Unauthorized action.');
        }
    }

    /*function test*/
    public function test(){
        /*INCLUDE LIBRARY SIMPLE DOM HTML */
        include(app_path().'\functions\simple_html_dom.php');
        /*END INCLUDE LIBRARY SIMPLE DOM HTML */
        $ch = curl_init("http://world-conect.com/feed");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $html = curl_exec($ch);
        curl_close($ch);
        $html = str_get_html($html);
        $data  = [];
        foreach ($html->find('item') as $item){
            $array = [];
            $array['title'] = $item->find('title', 0)->plaintext;
            $array['link'] = substr($item->find('comments' , 0)->plaintext , 0, strpos($item->find('comments' , 0)->plaintext , "#"));
            $array['pubDate'] = date('Y/m/d H:i:s' ,strtotime($item->find('pubDate', 0)->plaintext));
            $data[] = $array;
        }
        echo "<pre>";
        die(var_dump($data));
        return view('test.list');
    }
    /*end function test*/
}
