<?php

namespace App\Http\Controllers;

use App\Models\Attack;
use App\Models\Delivery_Request;
use App\Models\MailTemplate;
use App\Models\Memo;
use App\Models\Mtb_Attack_Status;
use App\Models\Price_List;
use App\Models\SuplierEmailAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Redirect;

class BuyerController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('userDeleted');
    }

    public function buyer_detail($attack_id){
        /* an_tnh_sc_94_start */
        $_free_user = Auth::user()->free_user;
        if ( $_free_user == '1' ) {
            return Redirect::to( '/free/buyer_detail/' . $attack_id );
        }
        $data['free_user'] = $_free_user;
        /* an_tnh_sc_94_end */
        
        $attack = Attack::find($attack_id);
        if (empty($attack)) {
            abort(404);
        }
        /*check permission*/
        if ($attack->supplier_id != Auth::user()->supplier->id) {
            abort(403);
        }
        $buyer = $attack->request->buyer;
        $supplier_id = Auth::user()->supplier->id;
        $data['attack'] = $attack;
        $data['buyer']  = $buyer;
        $data['mail_templates'] = MailTemplate::where('supplier_id',$supplier_id)->get();
        $data['price_lists'] = Price_List::where('supplier_id',$supplier_id)->get();
        $data['memo_list'] = Memo::where('attack_id',$attack_id)->orderBy('id', 'desc')->get();
        $data['mtb_attack_statuses'] = Mtb_Attack_Status::all();
        $data['sender_list'] = SuplierEmailAddress::where('supplier_id',$supplier_id)->get();
        return view('buyer.detail', $data);
    }

    /* sc-177 */
    public function delete_attack($attack_id){
        $supplier_id = Auth::user()->supplier->id;
        $attack = DB::table('attacks')->where('id',$attack_id)->first();
        if(!empty($attack)){
            if ($attack->supplier_id != Auth::user()->supplier->id) {
                abort(403);
            }
            else{
                $request_id = $attack->request_id;
                $delete_attack = DB::table('attacks')->where('id',$attack->id)->where('mtb_attack_status_id','=',1)->delete();
                if($delete_attack){
                    $delete_dashboard_buyer_masters = DB::table('dashboard_buyer_masters')->where('request_id',$request_id)->where('supplier_id',$supplier_id)->where('mtb_attack_status_id','=',1)->delete();
                    if($delete_dashboard_buyer_masters){
                        $count_information_row_bid = DB::table('dasboard_information_row')->where('supplier_id',$supplier_id)->value('status_bid');
                        $count_information_column_bid = DB::table('dasboard_information_column')->where('request_id',$request_id)->value('status_bid');
                        $count_information_row_bid = $count_information_row_bid - 1;
                        $count_information_column_bid = $count_information_column_bid - 1;
                        $update_information_row_bid = DB::table('dasboard_information_row')->where('supplier_id', $supplier_id)->update(['status_bid' => $count_information_row_bid]);
                        $update_information_column_bid = DB::table('dasboard_information_column')->where('request_id', $request_id)->update(['status_bid' => $count_information_column_bid]);
                    }
                }
                return redirect('/attack_list');
            }
        }
        else{
            abort(404);
        }
    }

    public function hidden_attack($attack_id){
        $supplier_id = Auth::user()->supplier->id;
        $attack = DB::table('attacks')->where('id',$attack_id)->first();
        if(!empty($attack)){
            if ($attack->supplier_id != Auth::user()->supplier->id) {
                abort(403);
            }
            else{
                if($attack->display_status == 0){
                    $update_display = DB::table('attacks')->where('id', $attack->id)->update(['display_status' => 1]);
                }
                else{
                    $update_display = DB::table('attacks')->where('id', $attack->id)->update(['display_status' => 0]);
                }
                return redirect('/attack_list');
            }
        }
        else{
            abort(404);
        }
    }
    /* end sc-177 */
}
