<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'flag_send_example' => 'required|integer|digits:1',
            'product_name' => ['required','max:255', Rule::unique('products')->where(function ($query){
                $query->where('supplier_id', Auth::user()->supplier->id);
            })->where('id','<>',$this->id)],
            'picture_main' => 'max:5120|image|dimensions:width=500,height=500',
            'picture_2' => 'max:5120|image|dimensions:width=500,height=500',
            'picture_3' => 'max:5120|image|dimensions:width=500,height=500',
            'picture_4' => 'max:5120|image|dimensions:width=500,height=500',
            'product_store_video_url' => 'url|max:255|one_byte',
            'product_description_url' => 'url|max:255|one_byte',
            'product_description' => 'required|max:65535',
            'fob_price' => 'required|integer|digits_between:1,11',
            'moq' => 'required|integer|digits_between:1,11',
            'length' => 'required|integer|digits_between:1,6',
            'width' => 'required|integer|digits_between:1,6',
            'height' => 'required|integer|digits_between:1,6',
            'packing_weight' => 'integer|digits_between:1,6',
            'sale_date' => 'required|date_format:Y-m-d',
            'sale_area' => 'required|max:65535',
            'deploy_ng_country' => 'required|max:65535',
            'example_price' => 'required|digits_between:1,11|integer',
            'different_point' => 'required|max:65535',
            'end_user_date_range' => 'array|required',
            'end_customer_gender' => 'array|required',
            'sale_channels' => 'array|required',
            'request_to_buyer' => 'array|required',
        ];
    }
    public function messages(){
        return [
        ];
    }
}
