<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSendProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        return [
//            'last_name' => 'required|max:255',
//            'first_name' => 'required|max:255',
//            'address' => 'required|max:255',
//            'city' => 'required|max:255',
//            'country' => 'required|max:255',
//            'company_name' => 'required|max:255',
//
//            'product_name_1' => 'max:255|required',
//            'price_1' => 'numeric|required',
//            'quantity_1' => 'integer|required',
//
//            'product_name_2' => 'max:255|required_with:price_2,quantity_2',
//            'price_2' => 'numeric|required_with:product_name_2,quantity_2',
//            'quantity_2' => 'integer|required_with:price_2,product_name_2',
//
//            'product_name_3' => 'max:255|required_with:price_3,quantity_3',
//            'price_3' => 'numeric|required_with:product_name_3,quantity_3',
//            'quantity_3' => 'integer|required_with:price_3,product_name_3',
//
//            'product_name_4' => 'max:255|required_with:price_4,quantity_4',
//            'price_4' => 'numeric|required_with:product_name_4,quantity_4',
//            'quantity_4' => 'integer|required_with:price_4,product_name_4',
//
//            'product_name_5' => 'max:255|required_with:price_5,quantity_5',
//            'price_5' => 'numeric|required_with:product_name_5,quantity_5',
//            'quantity_5' => 'integer|required_with:price_5,product_name_5',
//
//            'product_name_6' => 'max:255|required_with:price_6,quantity_6',
//            'price_6' => 'numeric|required_with:product_name_6,quantity_6',
//            'quantity_6' => 'integer|required_with:price_6,product_name_6',
//
//            'product_name_7' => 'max:255|required_with:price_7,quantity_7',
//            'price_7' => 'numeric|required_with:product_name_7,quantity_7',
//            'quantity_7' => 'integer|required_with:price_7,product_name_7',
//
//            'product_name_8' => 'max:255|required_with:price_8,quantity_8',
//            'price_8' => 'numeric|required_with:product_name_8,quantity_8',
//            'quantity_8' => 'integer|required_with:price_8,product_name_8',
//
//            'product_name_9' => 'max:255|required_with:price_9,quantity_9',
//            'price_9' => 'numeric|required_with:product_name_9,quantity_9',
//            'quantity_9' => 'integer|required_with:price_9,product_name_9',
//
//            'product_name_10' => 'max:255|required_with:price_10,quantity_10',
//            'price_10' => 'numeric|required_with:product_name_10,quantity_10',
//            'quantity_10' => 'integer|required_with:price_10,product_name_10',
//        ];
        $arr_validation = [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'company_name' => 'required|max:255',

            'product_name_1' => 'max:255|required',
            'price_1' => 'numeric|required',
            'quantity_1' => 'integer|required',

            'weight_1' => 'integer|required',
            'vertical_1' => 'integer|required',
            'width_1' => 'integer|required',
            'height_1' => 'integer|required',
            'box_number_1' => 'integer|required',
            'requested_delivery_date' => 'required|date_format:Y-m-d',
        ];
        for ($i = 2; $i <= 10; $i++) {
            $arr = [
                'weight_'.$i => 'integer|required_with:vertical_'.$i.',width_'.$i.',height_'.$i.',box_number_'.$i,
                'vertical_'.$i => 'integer|required_with:weight_'.$i.',width_'.$i.',height_'.$i.',box_number_'.$i,
                'width_'.$i => 'integer|required_with:vertical_'.$i.',weight_'.$i.',height_'.$i.',box_number_'.$i,
                'height_'.$i => 'integer|required_with:vertical_'.$i.',width_'.$i.',weight_'.$i.',box_number_'.$i,
                'box_number_'.$i => 'integer|required_with:vertical_'.$i.',width_'.$i.',height_'.$i.',weight_'.$i,

                'product_name_'.$i => 'max:255|required_with:price_'.$i.',quantity_'.$i,
                'price_'.$i => 'numeric|required_with:product_name_'.$i.',quantity_'.$i,
                'quantity_'.$i => 'integer|required_with:price_'.$i.',product_name_'.$i,
            ];
            $arr_validation = array_merge($arr_validation, $arr);
        }
        return $arr_validation;
    }
    public function messages()
    {
//        return [
//            'product_name_1.required_with' => trans('validation.required'),
//            'price_1.required_with' => trans('validation.required'),
//            'quantity_1.required_with' => trans('validation.required'),
//            'product_name_2.required_with' => trans('validation.required'),
//            'price_2.required_with' => trans('validation.required'),
//            'quantity_2.required_with' => trans('validation.required'),
//            'product_name_3.required_with' => trans('validation.required'),
//            'price_3.required_with' => trans('validation.required'),
//            'quantity_3.required_with' => trans('validation.required'),
//            'product_name_4.required_with' => trans('validation.required'),
//            'price_4.required_with' => trans('validation.required'),
//            'quantity_4.required_with' => trans('validation.required'),
//            'product_name_5.required_with' => trans('validation.required'),
//            'price_5.required_with' => trans('validation.required'),
//            'quantity_5.required_with' => trans('validation.required'),
//            'product_name_6.required_with' => trans('validation.required'),
//            'price_6.required_with' => trans('validation.required'),
//            'quantity_6.required_with' => trans('validation.required'),
//            'product_name_7.required_with' => trans('validation.required'),
//            'price_7.required_with' => trans('validation.required'),
//            'quantity_7.required_with' => trans('validation.required'),
//            'product_name_8.required_with' => trans('validation.required'),
//            'price_8.required_with' => trans('validation.required'),
//            'quantity_8.required_with' => trans('validation.required'),
//            'product_name_9.required_with' => trans('validation.required'),
//            'price_9.required_with' => trans('validation.required'),
//            'quantity_9.required_with' => trans('validation.required'),
//            'product_name_10.required_with' => trans('validation.required'),
//            'price_10.required_with' => trans('validation.required'),
//            'quantity_10.required_with' => trans('validation.required'),
//        ];

        $arr_messages = [];
        for ($i = 2; $i <= 10; $i++) {
            $arr = [
                'weight_'.$i.'.required_with' => trans('validation.required'),
                'vertical_'.$i.'.required_with' => trans('validation.required'),
                'width_'.$i.'.required_with' => trans('validation.required'),
                'height_'.$i.'.required_with' => trans('validation.required'),
                'box_number_'.$i.'.required_with' => trans('validation.required'),

                'product_name_'.$i.'.required_with' => trans('validation.required'),
                'price_'.$i.'.required_with' => trans('validation.required'),
                'quantity_'.$i.'.required_with' => trans('validation.required'),
            ];
            $arr_messages = array_merge($arr_messages, $arr);
        }
        return $arr_messages;

    }
}
