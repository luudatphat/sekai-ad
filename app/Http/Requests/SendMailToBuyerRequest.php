<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class SendMailToBuyerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mail_title' => 'required',
            'mail_content' => 'required',
            'mail_cc' => 'emails'
        ];
    }

    public function messages()
    {
        return [
            'mail_cc.emails' => '入力されたメールアドレスに问题があります。'
        ];
    }
}
