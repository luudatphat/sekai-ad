<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTemplateMail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'template_name' => 'required|max:255',
            'subject'       => 'required|max:255',
            'contentd'      => 'required',
            'attach_file'   => 'max:20480',
        ];
    }

    public function messages(){
        return [
        ];
    }
}
