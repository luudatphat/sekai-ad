<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Logout;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        $event->user->last_logout = date('Y-m-d H:i:s');
        $suplier = \App\Models\Supplier::where('user_id', '=', $event->user->id)->get()->first();
        $suplier->last_logout = date('Y-m-d H:i:s');
        $event->user->total_time_login = $event->user->total_time_login + ( strtotime( $event->user->last_logout ) - strtotime( $event->user->last_login ) );
        $suplier->total_time_login = $suplier->total_time_login + ( strtotime( $suplier->last_logout ) - strtotime( $suplier->last_login ) );
        $suplier->save();
        $event->user->save();
    }
}
