<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /* an_tnh_sc_94_start */
    protected $fillable = [
        'type', 'email', 'password', 'status', 'free_user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }
    public function mtb_role() {
        return $this->belongsTo('App\Models\Mtb_Role');
    }
    public function supplier(){
        return $this->hasOne('App\Models\Supplier');
    }
}
