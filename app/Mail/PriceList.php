<?php

namespace App\Mail;

use App\Models\Price_List;
use Barryvdh\DomPDF\Facade as PDF;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class PriceList extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $price_list;
    private $mail_subject;
    private $mail_cc;
    private $mail_content;
    private $mail_attach_file;
    private $mail_template_attach_file;
    private $price_list_id_list;
    private $mail_from;

    public function __construct($subject, $cc, $content, $attach_file, $mail_template_attach_file, $price_list_id_list, $from)
    {
        $this->mail_subject = $subject;
        $this->mail_cc = $cc;
        $this->mail_content = $content;
        $this->mail_attach_file = $attach_file;
        $this->mail_template_attach_file = $mail_template_attach_file;
        $this->price_list_id_list = $price_list_id_list;
        $this->mail_from = $from;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['content'] = $this->mail_content;
        $response = $this->from($this->mail_from)
            ->subject($this->mail_subject)
            ->view('emails.buyer',$data);
        /*CC*/
        if ($this->mail_cc !== NULL && $this->mail_cc != '') {
            $this->mail_cc[] = config('constants.MAIL_CC');
            $response->cc($this->mail_cc);
        }else{
            $response->cc(config('constants.MAIL_CC'));
        }
        /*attach price list*/
        if ($this->price_list_id_list !== NULL && count($this->price_list_id_list) > 0) {
            foreach ($this->price_list_id_list as $price_list_id) {
                $price_list = Price_List::find($price_list_id);
                $pdf_content = $this->pdf($price_list);
                $response->attachData($pdf_content, $price_list->price_list_name.'.pdf', [
                    'mime' => 'application/pdf',
                ]);
                /*Get pdf content to save to mail_attachments*/
                $price_list->pdf_content = $pdf_content;
                $response->price_list[] = $price_list;
            }
        }
        /*add mail attach file*/
        if ($this->mail_attach_file !== NULL) {
            foreach ($this->mail_attach_file as $item){
                $response->attachData($this->base64_file($item), $item->file_name, [
                    'mime' => $item->mime_type,
                ]);
            }
//            $response->attachData($this->base64_file($this->mail_attach_file), $this->mail_attach_file->file_name, [
//                'mime' => $this->mail_attach_file->mime_type,
//            ]);
        }
        /*add mail template attach file*/
        if ($this->mail_template_attach_file !== NULL) {
//            $response->attachData($this->base64_file($this->mail_template_attach_file), $this->mail_template_attach_file->file_name, [
//                'mime' => $this->mail_template_attach_file->mime_type,
//            ]);

        /* SC - 46 */
        foreach ($this->mail_template_attach_file as $item){
            $response->attachData($this->base64_file($item->media_files), $item->media_files->file_name, [
                'mime' => $item->media_files->file_type,
            ]);
        }
        /* End SC - 46*/

        }
        return $response;
    }
    /**
     * Raw pdf file.
     *
     * @return binary pdf file
     */
    protected function pdf($price_list)
    {
        $data['price_list'] = $price_list;
        $pdf = PDF::loadView('pdfs.pricelist', $data);
        return $pdf->stream();
    }

    protected function base64_file($attach_file)
    {
        return base64_decode($attach_file->content);
    }

}
