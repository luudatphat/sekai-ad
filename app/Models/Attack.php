<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attack extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function request() {
        return $this->belongsTo('App\Models\Request');
    }

    public function supplier() {
        return $this->belongsTo('App\Models\Supplier');
    }

    public function contract_report() {
        return $this->hasOne('App\Models\Contract_Report');
    }

    public function mtb_attack_status() {
        return $this->belongsTo('App\Models\Mtb_Attack_Status');
    }
}
