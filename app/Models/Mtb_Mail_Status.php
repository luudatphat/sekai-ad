<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Mail_Status extends Model
{
    protected $table = 'mtb_mail_statuses';
}
