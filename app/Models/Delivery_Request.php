<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery_Request extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'delivery_requests';

    public function suppliers(){
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }
}
