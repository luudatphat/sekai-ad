<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Sale_Channel extends Model
{
    protected $table = 'mtb_sale_channels';
}
