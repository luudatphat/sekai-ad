<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Media_File extends Model
{
    use SoftDeletes;
    protected $table = 'media_files';
    protected $dates = ['deleted_at'];
    /*Relationship one to one with Contract_Report model column invoice*/
    public function contract_report_invoice()
    {
        return $this->hasOne('App\Models\Contract_Report','invoice');
    }
    /*Relationship one to one with Contract_Report model column packing_list*/
    public function contract_report_packing_list()
    {
        return $this->hasOne('App\Models\Contract_Report','packing_list');
    }
    public function product_product_image_main()
    {
        return $this->hasOne('App\Models\Product','product_image_main');
    }

    public function product_product_image_2()
    {
        return $this->hasOne('App\Models\Product','product_image_2');
    }

    public function product_product_image_3()
    {
        return $this->hasOne('App\Models\Product','product_image_3');
    }

    public function product_product_image_4()
    {
        return $this->hasOne('App\Models\Product','product_image_4');
    }
    public function mail_template()
    {
        return $this->hasOne('App\Models\MailTemplate','attach_file');
    }
}
