<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Contract_Status extends Model
{
    protected $table = 'mtb_contract_statuses';
    /*Create table relationships*/
    public function suppliers(){
        return $this->hasMany('App\Models\Supplier');
    }
}
