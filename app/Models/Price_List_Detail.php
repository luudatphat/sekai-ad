<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price_List_Detail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = "price_list_detail";

    public function price_list() {
        return $this->belongsTo('App\Models\Price_List_Detail');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product');
    }
}
