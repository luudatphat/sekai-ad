<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier_Available_Category extends Model
{
    protected $table = 'supplier_available_categories';
    public function supplier() {
        return $this->belongsTo('App\Models\Supplier');
    }

    public function mtb_genre(){
        return $this->belongsTo('App\Models\Mtb_Genre');
    }
}
