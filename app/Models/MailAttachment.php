<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailAttachment extends Model
{
    public function mail(){
        return $this->belongsTo('App\Models\Mail');
    }

    public function media_files(){
        return $this->belongsTo('App\Models\Media_File','media_file_id');
    }
}
