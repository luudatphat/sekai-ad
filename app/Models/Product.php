<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function suppliers(){
        return $this->belongsTo('App\Models\Supplier');
    }

    public function mtb_date_ranges(){
        return $this->belongsToMany('App\Models\Mtb_Date_Range','product_date_ranges','product_id','mtb_date_range_id');
    }

    public function mtb_sale_channels(){
        return $this->belongsToMany('App\Models\Mtb_Sale_Channel','product_sale_channels','product_id','mtb_sale_channel_id');
    }

    public function mtb_request_to_buyers(){
        return $this->belongsToMany('App\Models\Mtb_Request_To_Buyer','product_request_to_buyers','product_id','mtb_request_to_buyer_id');
    }
    public function product_image_main_media_file()
    {
        return $this->belongsTo('App\Models\Media_File','product_image_main');
    }

    public function product_image_2_media_file()
    {
        return $this->belongsTo('App\Models\Media_File','product_image_2');
    }

    public function product_image_3_media_file()
    {
        return $this->belongsTo('App\Models\Media_File','product_image_3');
    }

    public function product_image_4_media_file()
    {
        return $this->belongsTo('App\Models\Media_File','product_image_4');
    }
}
