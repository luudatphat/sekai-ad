<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Title extends Model
{
    protected $table = 'mtb_titles';
}
