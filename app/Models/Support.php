<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Support extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    //
    public function mtb_support_categories() {
        return $this->belongsTo('App\Models\Mtb_Support_Category');
    }
}
