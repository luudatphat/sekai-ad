<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Posting_Progress extends Model
{
    protected $table = 'mtb_posting_progresses';
    /*Create table relationships*/
    public function suppliers(){
        return $this->hasMany('App\Models\Supplier');
    }
}
