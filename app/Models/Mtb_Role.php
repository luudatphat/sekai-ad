<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Role extends Model
{
    protected $table = 'mtb_roles';
    /*Create table relationships*/
    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
