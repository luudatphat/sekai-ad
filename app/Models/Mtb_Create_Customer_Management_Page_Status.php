<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Create_Customer_Management_Page_Status extends Model
{
    protected $table = 'mtb_create_customer_management_page_statuses';
    /*Create table relationships*/
    public function suppliers(){
        return $this->hasMany('App\Models\Supplier');
    }
}
