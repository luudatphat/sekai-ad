<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Support_Category extends Model
{
    //
    protected $table = 'mtb_support_categories';

    public function support(){
        return $this->hasMany('App\Models\Support');
    }
}
