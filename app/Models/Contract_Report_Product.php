<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract_Report_Product extends Model
{
    protected $table = 'contract_report_products';

     /**
      * Get the contract_report that owns the contract_report_product.
      */
     public function contract_report() {
         return $this->belongsTo('App\Models\Contract_Report','contract_report');
     }
    /**
     * Get the mtb_genre that owns the contract_report_product.
     */
    public function mtb_genre() {
        return $this->belongsTo('App\Models\Mtb_Genre');
    }
}
