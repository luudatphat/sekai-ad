<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Type extends Model
{
    protected $table = 'mtb_types';
    /*Create table relationships*/
    public function suppliers(){
        return $this->hasMany('App\Models\Supplier');
    }
}
