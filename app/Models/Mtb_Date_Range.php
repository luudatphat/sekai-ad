<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Date_Range extends Model
{
    protected $table = 'mtb_date_ranges';
}
