<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Price_List extends Model
{
    use SoftDeletes;
    protected $table = "price_list";
    protected $dates = ['deleted_at'];

    public function price_list_details(){
        return $this->hasMany('App\Models\Price_List_Detail','price_list_id');
    }
}
