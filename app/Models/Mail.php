<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function mail_attachmennt(){
        return $this->hasMany('App\Models\MailAttachment');
    }

    public function attack() {
        return $this->belongsTo('App\Models\Attack', 'attack_id');
    }

    public function supplier() {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }
}
