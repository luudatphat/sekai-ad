<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Buyer extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public function mtb_title() {
        return $this->belongsTo('App\Models\Mtb_Title');
    }

    public function mtb_sns_tool() {
        return $this->belongsTo('App\Models\Mtb_Sns_Tool');
    }

    public function mtb_business_categories(){
        return $this->belongsToMany('App\Models\Mtb_Business_Category','buyer_business_categories','buyer_id','mtb_business_category_id');
    }

    public function mtb_goods_categories(){
        return $this->belongsToMany('App\Models\Mtb_Goods_Category','buyer_handled_goods','buyer_id','mtb_goods_category_id');
    }

    public function countries() {
        return $this->belongsTo('App\Models\Country','country');
    }

    public function requests() {
        return $this->hasMany('App\Models\Request');
    }
}
