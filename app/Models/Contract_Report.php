<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract_Report extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'contract_reports';
    /**
     * Get the contract_report_products for the contract_report.
     */
    public function contract_report_products()
    {
        return $this->hasMany('App\Models\Contract_Report_Product','contract_report_id');
    }

    public function invoice_media_file()
    {
        return $this->belongsTo('App\Models\Media_File','invoice');
    }

    public function packing_list_media_file()
    {
        return $this->belongsTo('App\Models\Media_File','packing_list');
    }

    public function attacks(){
        return $this->belongsTo('App\Models\Attack','attack_id');
    }
}
