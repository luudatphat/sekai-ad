<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Sns_Tool extends Model
{
    protected $table = 'mtb_sns_tools';
}
