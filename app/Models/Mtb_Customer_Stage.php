<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Customer_Stage extends Model
{
    protected $table = 'mtb_customer_stages';
    /*Create table relationships*/
    public function suppliers(){
        return $this->hasMany('App\Models\Supplier');
    }
}
