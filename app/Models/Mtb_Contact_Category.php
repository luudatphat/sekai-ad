<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mtb_Contact_Category extends Model
{
    protected $table = 'mtb_contact_categories';
}
