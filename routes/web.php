<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\Models\Price_List;
use App\Models\Price_List_Detail;
use App\Models\Media_File;
use App\Models\Product;


Auth::routes();

//Route::get('/', 'HomeController@index');

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@home');

Route::post('/home_4.html','HomeController@home_4');

Route::get('/news', 'HomeController@news');
Route::get('/news/details/{id}', 'HomeController@details');
/*Supplier*/
Route::get('/profile','SupplierController@edit');

Route::post('/profile','SupplierController@edit_handler');

/*ReportController*/
Route::get('/report_list','ReportController@report_list');


Route::get('/report_detail/{id}','ReportController@report_detail');

Route::get('/report_create','ReportController@create_report');

Route::post('/report_create','ReportController@create_report_handler');

/*ProductController*/
Route::get('/product_list', 'ProductController@product_list');

Route::post('/product_list', 'ProductController@product_list');

Route::post('/list_product_sort', 'ProductController@sort');

Route::get('/create_product', 'ProductController@create_product');

Route::post('/create_product', 'ProductController@create_product_handler');

Route::get('/product_detail/{id}', 'ProductController@product_detail');

Route::get('/product_edit/{id}', 'ProductController@product_edit');

Route::post('/update_product', 'ProductController@product_update');

/*Request*/

Route::get('/request_list','RequestController@request_list');

Route::post('/request_list','RequestController@request_list');

Route::post('/list_request_sort', 'RequestController@sort');
/*Attack*/
Route::get('/attack_list', 'AttackController@attack_list');

Route::post('/attack_list', 'AttackController@attack_list');

Route::post('/new_attack','AttackController@new_attack');

Route::post('/change_attack_status','AttackController@change_attack_status');

Route::post('/list_attack_sort','AttackController@sort');

Route::post('/change_status_agency','AttackController@change_status_agency');
/*Buyer*/
Route::get('/buyer_detail/{attack_id}', 'BuyerController@buyer_detail');

/*Template mail*/
Route::get('/template_mail_list','MailTemplateController@index');

Route::get('/template_mail_create','MailTemplateController@create_new_template');

Route::post('/template_mail_create','MailTemplateController@create_new_template_handler');

Route::get('/template_mail_detail/{id}','MailTemplateController@edit_template_mail');

Route::post('/template_mail_detail/{id}','MailTemplateController@update_template_mail');

Route::get('/template_mail_delete/{id}','MailTemplateController@delete_template_mail');

Route::post('/load_mail_template','MailTemplateController@load_mail_template');
/*End Template mail*/

/*ContactController*/
Route::get('/contact','SupportController@create_support');
Route::post('/contact','SupportController@create_support_handler');
/*MediaFileController*/
Route::get('/report_download/{file_type}/{media_file_id}', 'MediaFileController@download_report');
Route::get('/image/{media_file_id}', 'MediaFileController@display_image');

/*Price list*/
Route::get('/price_list','Price_List_Controller@index');
Route::get('/create_new_price_list_detail','Price_List_Controller@create_new_price_list_detail');
Route::get('/delete_price_list/{id}','Price_List_Controller@delete_price_list');
Route::get('/edit_price_list_detail/{id}','Price_List_Controller@edit_price_list_detail');
Route::get('/download_file_pdf/{id}','Price_List_Controller@download_file_pdf');
Route::get('/download_file_excel/{id}','Price_List_Controller@download_file_excel');

Route::get('/test','Price_List_Controller@test');

Route::post('/create_new_price_list_detail','Price_List_Controller@create_new_price_list_detail_handler');
Route::post('/delete_price_list_detail','Price_List_Controller@delete_price_list_detail');
Route::post('/save_diff_name','Price_List_Controller@save_diff_name');
Route::post('/get_product_list','Price_List_Controller@get_product_list');

/*MemoController*/
Route::post('/add_memo','MemoController@add_memo');
/*MailController*/
Route::post('/send_email_to_buyer','MailController@send_email_to_buyer');
/* Translate */
Route::get('/translate-list','TranslateController@translate_list');
Route::post('/translate-list','TranslateController@translate_list');
Route::post('/translate-sort','TranslateController@sort');
Route::get('/create-translate-request','TranslateController@create');
Route::post('/create-translate-request','TranslateController@create_handler');
Route::get('/translate-detail/{translate_id}','TranslateController@edit_translate');
Route::post('/translate-detail/{translate_id}','TranslateController@edit_translate_handler');
Route::delete('delete-translate-request','TranslateController@delete_translate');
/* End Translate */

/* Send Product */
Route::get('/send-product','SendProductController@index');
Route::post('/send-product','SendProductController@index');
Route::post('/send-product-sort','SendProductController@sort');
Route::get('/create-send-product','SendProductController@create');
Route::post('/create-send-product','SendProductController@create_handler');
Route::get('/detail-send-product/{id}','SendProductController@detail');
Route::post('/send-product-update','SendProductController@update');
/* Importer*/
Route::get('/importer_supplier','ImporterController@importer_supplier');
Route::get('/update_kana','ImporterController@update_kana_contact');
Route::get('/import_buyer','ImporterController@importer_buyer');
/* End Importer*/

/* Po Graph */
Route::get('/graph.html',function (){
    return view('home.graph1');
})->middleware('auth');
/* End Po Graph */
/*Graph*/
Route::post('load_graph_3','HomeController@load_graph_3');
Route::post('/graph/get_data_graph_4','HomeController@get_data_graph_handler');

Route::get('/po_mail',function (){
    return view('mails.list');
});

Route::get('/mail-list','MailController@index');
Route::post('/mail-list','MailController@index');
Route::post('/mail-sort','MailController@sort');
Route::get('/mail-detail/{mail_id}','MailController@edit');
Route::get('/mail-download/{mail_attachment_id}', 'MediaFileController@download_mail_attach_file');
Route::get('/mail-template-download/{media_file_id}', 'MediaFileController@download_mail_template_attach_file');

Route::get('/form-create-buyer',function(){
    return view('api.createbuyer');
});

Route::get('/form-create-request',function(){
    return view('api.createrequest');
});

Route::get('/interim',function(){
    return view('api.interim');
});

Route::get('/login-company/{id}/{temp_pass}',function(\Illuminate\Http\Request $request){

    $redirectTo = '/news';
    if(isset($request->id)&&isset($request->temp_pass)){
        $temp_login = \App\Models\TempLogin::where('user_id',$request->id)
            ->where('password_temp',$request->temp_pass)->get()->first();
        $user_deleted = \App\Models\User::find($temp_login->user_id);
        if($user_deleted->deleted_at != null){
            return redirect('/login');
        }
        if(isset($temp_login)){
            $delta_time = time() - strtotime($temp_login->created_at);
            $time = date('s', $delta_time);
            if($time <= 30)
            {
                Auth::loginUsingId($temp_login->user_id, true);
            }
            $temp = \App\Models\TempLogin::find($temp_login->id);
            $temp->delete();
        }
    }
    return redirect($redirectTo);
});

Route::get('/thanks',function(){
    return view('api.thanks');
});
//Route::post('/create-buyer','ApiController@createCompanyForeign');
//Route::post('/create-request','ApiController@createRequest');

Route::post('/create-buyer-by-form','ApiController@createCompanyForeign');
Route::post('/create-request-by-form','ApiController@createRequest');



/* For Free User */
Route::get('/register-free', function () {
    if($user = Auth::user())
    {
        return redirect( '/news' );
    }
    return view('free.register.register');
});
/* Home */
Route::get('/free/home', 'free\FreeHomeController@home');
/* News */
Route::get('/free/news', 'free\FreeHomeController@news');
Route::get('/free/news/details/{id}', 'free\FreeHomeController@details');
/* Attack List */
Route::get('/free/attack_list', 'free\FreeAttackController@attack_list');
Route::post('/free/attack_list', 'free\FreeAttackController@attack_list');
Route::post('/free/list_attack_sort','free\FreeAttackController@sort');
Route::get('/free/buyer_detail/{attack_id}', 'free\FreeBuyerController@buyer_detail');
/* Terms of service */
Route::get('/terms-of-service', 'TermsController@terms_of_service');
/* Request list */
Route::get('/free/request_list', 'free\FreeRequestController@request_list');
Route::post('/free/request_list', 'free\FreeRequestController@request_list');
/* Mail list */
Route::get('/free/mail-list', 'free\FreeMailController@index');
Route::post('/free/mail-list', 'free\FreeMailController@index');
Route::post('/free/mail-sort', 'free\FreeMailController@sort');
Route::get('/free/mail-detail/{mail_id}','free\FreeMailController@edit');
Route::get('/free/mail-download/{mail_attachment_id}', 'free\FreeMediaFileController@download_mail_attach_file');
/* Template mail list */
Route::get('/free/template_mail_list','free\FreeMailTemplateController@index');
Route::get('/free/template_mail_detail/{id}','free\FreeMailTemplateController@edit_template_mail');
Route::post('/free/template_mail_detail/{id}','free\FreeMailTemplateController@edit_template_mail');
Route::get('/free/template_mail_create','free\FreeMailTemplateController@create_new_template');
Route::post('/free/template_mail_create','free\FreeMailTemplateController@create_new_template_handler');
/* Translate list */
Route::get('/free/translate-list','free\FreeTranslateController@translate_list');
Route::post('/free/translate-list','free\FreeTranslateController@translate_list');
Route::post('/free/translate-sort','free\FreeTranslateController@sort');
Route::get('/free/create-translate-request','free\FreeTranslateController@create');
Route::post('/free/create-translate-request','free\FreeTranslateController@create_handler');
Route::get('/free/translate-detail/{translate_id}','free\FreeTranslateController@edit_translate');
Route::post('/free/translate-detail/{translate_id}','free\FreeTranslateController@edit_translate_handler');
Route::delete('/free/delete-translate-request','free\FreeTranslateController@delete_translate');
/*sc-177*/
Route::get('/delete_attack/{attack_id}', 'BuyerController@delete_attack');
Route::get('/hidden_attack/{attack_id}', 'BuyerController@hidden_attack');