<?php

return [
    'create_product_success' => '新規な製品を正常に作成しました。',
    'create_report_success' => '新規なレポートを正常に作成しました。',
    'save_success' => '保存に成功しました。',
    'create_new_template_mail_success'  => '新規なテンプレートメールを正常に作成しました。',
    'update_template_mail_success'  => 'テンプレートメールの更新を正常に作成しました。',
    'delete_template_mail_success'  => 'テンプレートメールの更新を正常に削除しました。',
    'detail_update_template_name'   => 'テンプレートメール名は必須です。',
    'detail_update_subject'         => 'テンプレートメール件名は必須です。',
    'detail_update_content'         => 'テンプレートメールの内容は必須です。',
    'header_template_mail_name'     => 'テンプレートメール名',
    'header_template_mail_subject'  => 'テンプレートメール件名',
    'header_template_mail_content'  => 'テンプレートメールの内容',
    'price_list_delete_success'     => '価格リストの削除を正常に削除しました。',
    'send_mail_success'     => 'メールを送信しました',
    'create_translate_request_success' => '新規な翻訳要求を正常に作成しました。',
    'update_translate_request_success' => '翻訳要求を正常に保存しました。',
    'send_translate_request_success' => '翻訳要求を正常に送信しました。',
    'create_support_success' => 'メッセージを正常に送信しました。',
    'update_delivery_success' => '配信要求を正常に保存しました。',
    'attack_success' => 'アタックリストに追加しました。',
    'attack_confirm' => 'アタックリストに追加してもよろしいですか。',
    'delete_translate_request_confirm' => 'この翻訳リクエストを削除してもよろしいですか？',
    'add_memo_success' => '新しいメモを正常に追加します。',
    /*Error*/
    'file_upload_error' => 'ファイルのアップロードエラー',
    'error' => 'エラーが発生しました。 後で再度お試しください。',
    'mail_error' => 'メール送信エラー。 後で再度お試しください。',
    'create_contract_report_before_change_attack_status_error' => 'リピート成約済に状態を変更する前に契約報告書を作成してください。'
];
