@extends('layouts.master')
@section('sekai_content')
    <h2>メールでのお問い合わせ</h2>
    <div class="container container_contact">
        <form action="" method="" name="">
            <table class="table table-bordered table_contact_po">
                <tr>
                    <td width="350" style="background-color: #fafafa;    vertical-align: middle;" class="text-center">問い合わせ内容 <span class="text-danger">必須</span></td>
                    <td>
                        <div class="radio">
                            <label><input type="radio" name="optradio" checked>商談方法について</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio">商品掲載について</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio">その他</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="350" style="background-color: #fafafa;    vertical-align: middle;" class="text-center">詳細</td>
                    <td>
                        <textarea class="form-control" name="" rows="10"></textarea>
                    </td>
                </tr>
            </table>
            <!-- Button submit -->
            <div class="row row_create_report text-center">
                <input id="btn_submit_po" class="btn btn-warning" type="submit" name="submit" value="送信する">
            </div>
        </form>
    </div>
@endsection