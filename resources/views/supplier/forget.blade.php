@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h1 class="h1 bg-primary text-center h1_list_user">パスワードリセット要求</h1>
        <form action="" class="form-group">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <label for="forget">登録したメールアドレス <span class="text-danger">必須</span></label>
                    <input type="email" class="form-control" id="forget" placeholder="">
                </div>
                <div class="col-md-6 col-md-offset-3 btn_login_company text-center">
                    <input type="submit" class="btn btn-primary" value="Reset">
                </div>
            </div>
        </form>
    </div>
@endsection