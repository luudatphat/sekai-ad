<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<?php

$style = [
        "table" => "border: 1px solid #000000;"
        ];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

<body style="">
<h1>価格表一覧</h1>
<table>

    <tr style="{{ $style['table'] }}">
        <th align="center">価格表名</th>
        <th align="center">最終更新日</th>
    </tr>

    <tbody>
        <tr>
            <td align="center" style="{{ $style['table'] }}">{{$price_list[0]->price_list_name}}</td>
            <td align="center" style="{{ $style['table'] }}">
                <?php
                    $str = $price_list[0]->updated_at;
                    $str = substr($str , 0 , strpos($str , ":")-3);
                    $str = explode("-", $str);
                    $final = $str[0]."年".$str[1]."月".$str[2]."日";
                    echo $final;
                ?>
            </td>
        </tr>
    </tbody>
</table>

<table>
    <tr style="{{ $style['table'] }}">
        <th style="height: 50px;" align="center">商品写真</th>
        <th style="height: 50px;" align="center">英文商品名</th>
        <th style="height: 50px;wrap-text:true;" align="center">ケース入数</th>
        <th style="height: 50px;wrap-text:true;" align="center">MOQ/Piece</th>
        <th style="height: 50px;wrap-text:true;" align="center">商品価格</th>
        <th style="height: 50px;wrap-text:true;" align="center">ケース当たりSize 縦 (CM)</th>
        <th style="height: 50px;wrap-text:true;" align="center">ケース当たりSize 横 (CM)</th>
        <th style="height: 50px;wrap-text:true;" align="center">ケース当たりSize 高さ(CM)</th>
        <th style="height: 50px;wrap-text:true;" align="center">ケース当たり重量</th>
    </tr>

    <tbody>
    @foreach($price_list_detail as $key => $list)
        <tr>
            <td style="{{ $style['table'] }}" width="50%" height="100px"></td>
            <td width="50px" style="{{ $style['table'] }}" align="center">{{$list->name_product_en}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_fob_price}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_moq}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_input}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_length}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_width}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_height}}</td>
            <td width="15px" style="{{ $style['table'] }}" align="center">{{$list->price_list_packing_weight}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>
