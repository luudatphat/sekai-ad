<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/welcart_25hoursbuyers/style.css">
    <title>Registration | 25HOURS BUYERS.com</title>
    <?php include resource_path('views/layouts/juiser.php'); ?>
    <?php include resource_path('views/layouts/push-notification.php'); ?>
</head>
<body>

<div id="mainContent">
    <div class="wrap">

        <div id="content" style="width: auto;float: none">
            <section id="page">

                <h2 id="pageTtl">
                    Registration
                </h2>

                <p>
                    Please register by entering the required information in the following fields.<br>
                    You can start using our website after we have confirmed your details.<br>
                    <font color="red">***Please enter information in lower-case letters (e.g. Aaaaa Bbbbb)***</font>
                </p>

                <div id="form-layout">

                    <form action="/create-buyer-by-form" method="post">
                        <table>
                            <tr>
                                <th><label for="questions_176815">Company name <span class="komejirushi">(required)</span></label></th>
                                <td><input type="text" name="questions[176815]" class="inputTextSingle" id="questions_176815" /><br />*64 or less characters</td>
                            </tr>
                            <tr>
                                <th><label for="questions_176816">Title <span class="komejirushi">(required)</span></label></th>
                                <td><ul class="radio_list"><li><input name="questions[176816]" type="radio" value="1059191" id="questions_176816_1059191" checked="checked" />&nbsp;<label for="questions_176816_1059191">Mr</label></li>
                                        <li><input name="questions[176816]" type="radio" value="1059192" id="questions_176816_1059192" />&nbsp;<label for="questions_176816_1059192">Miss</label></li>
                                        <li><input name="questions[176816]" type="radio" value="1059193" id="questions_176816_1059193" />&nbsp;<label for="questions_176816_1059193">Mrs</label></li>
                                        <li><input name="questions[176816]" type="radio" value="29111994" id="questions_176816_29111994" />&nbsp;<label for="questions_176816_29111994">Ms</label></li></ul></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176817">First name <span class="komejirushi">(required)</span></label></th>
                                <td><input type="text" name="questions[176817]" class="inputTextSingle" id="questions_176817" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176818">Last name <span class="komejirushi">(required)</span></label></th>
                                <td><input type="text" name="questions[176818]" class="inputTextSingle" id="questions_176818" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176819">Country  <span class="komejirushi">(required)</span></label></th>
                                <!--                                    <td><input type="text" name="country" class="inputTextSingle" id="country" value="" /></td>-->
                                <td>
                                    <div class="ui-widget">
                                        <select name="country" id="country" >
                                            <option value="" selected="selected">-----</option>
                                            <option value='AF'>Afghanistan</option>
                                            <option value='AL'>Albania</option>
                                            <option value='DZ'>Algeria</option>
                                            <option value='DS'>American Samoa</option>
                                            <option value='AD'>Andorra</option>
                                            <option value='AO'>Angola</option>
                                            <option value='AI'>Anguilla</option>
                                            <option value='AQ'>Antarctica</option>
                                            <option value='AG'>Antigua and Barbuda</option>
                                            <option value='AR'>Argentina</option>
                                            <option value='AM'>Armenia</option>
                                            <option value='AW'>Aruba</option>
                                            <option value='AU'>Australia</option>
                                            <option value='AT'>Austria</option>
                                            <option value='AZ'>Azerbaijan</option>
                                            <option value='BS'>Bahamas</option>
                                            <option value='BH'>Bahrain</option>
                                            <option value='BD'>Bangladesh</option>
                                            <option value='BB'>Barbados</option>
                                            <option value='BY'>Belarus</option>
                                            <option value='BE'>Belgium</option>
                                            <option value='BZ'>Belize</option>
                                            <option value='BJ'>Benin</option>
                                            <option value='BM'>Bermuda</option>
                                            <option value='BT'>Bhutan</option>
                                            <option value='BO'>Bolivia</option>
                                            <option value='BA'>Bosnia and Herzegovina</option>
                                            <option value='BW'>Botswana</option>
                                            <option value='BV'>Bouvet Island</option>
                                            <option value='BR'>Brazil</option>
                                            <option value='IO'>British Indian Ocean Territory</option>
                                            <option value='BN'>Brunei Darussalam</option>
                                            <option value='BG'>Bulgaria</option>
                                            <option value='BF'>Burkina Faso</option>
                                            <option value='BI'>Burundi</option>
                                            <option value='KH'>Cambodia</option>
                                            <option value='CM'>Cameroon</option>
                                            <option value='CA'>Canada</option>
                                            <option value='CV'>Cape Verde</option>
                                            <option value='KY'>Cayman Islands</option>
                                            <option value='CF'>Central African Republic</option>
                                            <option value='TD'>Chad</option>
                                            <option value='CL'>Chile</option>
                                            <option value='CN'>China</option>
                                            <option value='CX'>Christmas Island</option>
                                            <option value='CC'>Cocos (Keeling) Islands</option>
                                            <option value='CO'>Colombia</option>
                                            <option value='KM'>Comoros</option>
                                            <option value='CG'>Congo</option>
                                            <option value='CK'>Cook Islands</option>
                                            <option value='CR'>Costa Rica</option>
                                            <option value='HR'>Croatia (Hrvatska)</option>
                                            <option value='CU'>Cuba</option>
                                            <option value='CY'>Cyprus</option>
                                            <option value='CZ'>Czech Republic</option>
                                            <option value='DK'>Denmark</option>
                                            <option value='DJ'>Djibouti</option>
                                            <option value='DM'>Dominica</option>
                                            <option value='DO'>Dominican Republic</option>
                                            <option value='TP'>East Timor</option>
                                            <option value='EC'>Ecuador</option>
                                            <option value='EG'>Egypt</option>
                                            <option value='SV'>El Salvador</option>
                                            <option value='GQ'>Equatorial Guinea</option>
                                            <option value='ER'>Eritrea</option>
                                            <option value='EE'>Estonia</option>
                                            <option value='ET'>Ethiopia</option>
                                            <option value='FK'>Falkland Islands (Malvinas)</option>
                                            <option value='FO'>Faroe Islands</option>
                                            <option value='FJ'>Fiji</option>
                                            <option value='FI'>Finland</option>
                                            <option value='FR'>France</option>
                                            <option value='FX'>France, Metropolitan</option>
                                            <option value='GF'>French Guiana</option>
                                            <option value='PF'>French Polynesia</option>
                                            <option value='TF'>French Southern Territories</option>
                                            <option value='GA'>Gabon</option>
                                            <option value='GM'>Gambia</option>
                                            <option value='GE'>Georgia</option>
                                            <option value='DE'>Germany</option>
                                            <option value='GH'>Ghana</option>
                                            <option value='GI'>Gibraltar</option>
                                            <option value='GK'>Guernsey</option>
                                            <option value='GR'>Greece</option>
                                            <option value='GL'>Greenland</option>
                                            <option value='GD'>Grenada</option>
                                            <option value='GP'>Guadeloupe</option>
                                            <option value='GU'>Guam</option>
                                            <option value='GT'>Guatemala</option>
                                            <option value='GN'>Guinea</option>
                                            <option value='GW'>Guinea-Bissau</option>
                                            <option value='GY'>Guyana</option>
                                            <option value='HT'>Haiti</option>
                                            <option value='HM'>Heard and Mc Donald Islands</option>
                                            <option value='HN'>Honduras</option>
                                            <option value='HK'>Hong Kong</option>
                                            <option value='HU'>Hungary</option>
                                            <option value='IS'>Iceland</option>
                                            <option value='IN'>India</option>
                                            <option value='IM'>Isle of Man</option>
                                            <option value='ID'>Indonesia</option>
                                            <option value='IR'>Iran (Islamic Republic of)</option>
                                            <option value='IQ'>Iraq</option>
                                            <option value='IE'>Ireland</option>
                                            <option value='IL'>Israel</option>
                                            <option value='IT'>Italy</option>
                                            <option value='CI'>Ivory Coast</option>
                                            <option value='JE'>Jersey</option>
                                            <option value='JM'>Jamaica</option>
                                            <option value='JP'>Japan</option>
                                            <option value='JO'>Jordan</option>
                                            <option value='KZ'>Kazakhstan</option>
                                            <option value='KE'>Kenya</option>
                                            <option value='KI'>Kiribati</option>
                                            <option value='KP'>North Korea</option>
                                            <option value='KR'>South Korea</option>
                                            <option value='XK'>Kosovo</option>
                                            <option value='KW'>Kuwait</option>
                                            <option value='KG'>Kyrgyzstan</option>
                                            <option value='LA'>Lao People''s Democratic Republic</option>
                                            <option value='LV'>Latvia</option>
                                            <option value='LB'>Lebanon</option>
                                            <option value='LS'>Lesotho</option>
                                            <option value='LR'>Liberia</option>
                                            <option value='LY'>Libyan Arab Jamahiriya</option>
                                            <option value='LI'>Liechtenstein</option>
                                            <option value='LT'>Lithuania</option>
                                            <option value='LU'>Luxembourg</option>
                                            <option value='MO'>Macau</option>
                                            <option value='MK'>Macedonia</option>
                                            <option value='MG'>Madagascar</option>
                                            <option value='MW'>Malawi</option>
                                            <option value='MY'>Malaysia</option>
                                            <option value='MV'>Maldives</option>
                                            <option value='ML'>Mali</option>
                                            <option value='MT'>Malta</option>
                                            <option value='MH'>Marshall Islands</option>
                                            <option value='MQ'>Martinique</option>
                                            <option value='MR'>Mauritania</option>
                                            <option value='MU'>Mauritius</option>
                                            <option value='TY'>Mayotte</option>
                                            <option value='MX'>Mexico</option>
                                            <option value='FM'>Micronesia, Federated States of</option>
                                            <option value='MD'>Moldova, Republic of</option>
                                            <option value='MC'>Monaco</option>
                                            <option value='MN'>Mongolia</option>
                                            <option value='ME'>Montenegro</option>
                                            <option value='MS'>Montserrat</option>
                                            <option value='MA'>Morocco</option>
                                            <option value='MZ'>Mozambique</option>
                                            <option value='MM'>Myanmar</option>
                                            <option value='NA'>Namibia</option>
                                            <option value='NR'>Nauru</option>
                                            <option value='NP'>Nepal</option>
                                            <option value='NL'>Netherlands</option>
                                            <option value='AN'>Netherlands Antilles</option>
                                            <option value='NC'>New Caledonia</option>
                                            <option value='NZ'>New Zealand</option>
                                            <option value='NI'>Nicaragua</option>
                                            <option value='NE'>Niger</option>
                                            <option value='NG'>Nigeria</option>
                                            <option value='NU'>Niue</option>
                                            <option value='NF'>Norfolk Island</option>
                                            <option value='MP'>Northern Mariana Islands</option>
                                            <option value='NO'>Norway</option>
                                            <option value='OM'>Oman</option>
                                            <option value='PK'>Pakistan</option>
                                            <option value='PW'>Palau</option>
                                            <option value='PS'>Palestine</option>
                                            <option value='PA'>Panama</option>
                                            <option value='PG'>Papua New Guinea</option>
                                            <option value='PY'>Paraguay</option>
                                            <option value='PE'>Peru</option>
                                            <option value='PH'>Philippines</option>
                                            <option value='PN'>Pitcairn</option>
                                            <option value='PL'>Poland</option>
                                            <option value='PT'>Portugal</option>
                                            <option value='PR'>Puerto Rico</option>
                                            <option value='QA'>Qatar</option>
                                            <option value='RE'>Reunion</option>
                                            <option value='RO'>Romania</option>
                                            <option value='RU'>Russian Federation</option>
                                            <option value='RW'>Rwanda</option>
                                            <option value='KN'>Saint Kitts and Nevis</option>
                                            <option value='LC'>Saint Lucia</option>
                                            <option value='VC'>Saint Vincent and the Grenadines</option>
                                            <option value='WS'>Samoa</option>
                                            <option value='SM'>San Marino</option>
                                            <option value='ST'>Sao Tome and Principe</option>
                                            <option value='SA'>Saudi Arabia</option>
                                            <option value='SN'>Senegal</option>
                                            <option value='RS'>Serbia</option>
                                            <option value='SC'>Seychelles</option>
                                            <option value='SL'>Sierra Leone</option>
                                            <option value='SG'>Singapore</option>
                                            <option value='SK'>Slovakia</option>
                                            <option value='SI'>Slovenia</option>
                                            <option value='SB'>Solomon Islands</option>
                                            <option value='SO'>Somalia</option>
                                            <option value='ZA'>South Africa</option>
                                            <option value='GS'>South Georgia South Sandwich Islands</option>
                                            <option value='ES'>Spain</option>
                                            <option value='LK'>Sri Lanka</option>
                                            <option value='SH'>St. Helena</option>
                                            <option value='PM'>St. Pierre and Miquelon</option>
                                            <option value='SD'>Sudan</option>
                                            <option value='SR'>Suriname</option>
                                            <option value='SJ'>Svalbard and Jan Mayen Islands</option>
                                            <option value='SZ'>Swaziland</option>
                                            <option value='SE'>Sweden</option>
                                            <option value='CH'>Switzerland</option>
                                            <option value='SY'>Syrian Arab Republic</option>
                                            <option value='TW'>Taiwan</option>
                                            <option value='TJ'>Tajikistan</option>
                                            <option value='TZ'>Tanzania, United Republic of</option>
                                            <option value='TH'>Thailand</option>
                                            <option value='TG'>Togo</option>
                                            <option value='TK'>Tokelau</option>
                                            <option value='TO'>Tonga</option>
                                            <option value='TT'>Trinidad and Tobago</option>
                                            <option value='TN'>Tunisia</option>
                                            <option value='TR'>Turkey</option>
                                            <option value='TM'>Turkmenistan</option>
                                            <option value='TC'>Turks and Caicos Islands</option>
                                            <option value='TV'>Tuvalu</option>
                                            <option value='UG'>Uganda</option>
                                            <option value='UA'>Ukraine</option>
                                            <option value='AE'>United Arab Emirates</option>
                                            <option value='GB'>United Kingdom</option>
                                            <option value='US'>United States</option>
                                            <option value='UM'>United States minor outlying islands</option>
                                            <option value='UY'>Uruguay</option>
                                            <option value='UZ'>Uzbekistan</option>
                                            <option value='VU'>Vanuatu</option>
                                            <option value='VA'>Vatican City State</option>
                                            <option value='VE'>Venezuela</option>
                                            <option value='VN'>Vietnam</option>
                                            <option value='VG'>Virgin Islands (British)</option>
                                            <option value='VI'>Virgin Islands (U.S.)</option>
                                            <option value='WF'>Wallis and Futuna Islands</option>
                                            <option value='EH'>Western Sahara</option>
                                            <option value='YE'>Yemen</option>
                                            <option value='ZR'>Zaire</option>
                                            <option value='ZM'>Zambia</option>
                                            <option value='ZW'>Zimbabwe</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th><label for="questions_176820">Address <span class="komejirushi">(required)</span></label></th>
                                <td><input type="text" name="questions[176820]" class="inputTextSingle" id="questions_176820" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176821">Phone number <span class="komejirushi">(required)</span></label></th>
                                <td><input type="text" name="questions[176821]" class="inputTextSingle" id="questions_176821" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176822">Mobile number</label></th>
                                <td><input type="text" name="questions[176822]" class="inputTextSingle" id="questions_176822" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176823">E-Mail address <span class="komejirushi">(required)</span></label></th>
                                <td><input style="ime-mode: disabled;" type="text" name="questions[176823]" class="inputTextSingle" id="questions_176823" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176824">SNS tool <span class="komejirushi">(required)</span></label></th>
                                <td><select name="questions[176824]" id="questions_176824">
                                        <option value="" selected="selected">-----</option>
                                        <option value="1059194">WhatsApp</option>
                                        <option value="1059195">WeChat</option>
                                        <option value="1059196">LINE</option>
                                        <option value="1059197">Facebook Messenger</option>
                                        <option value="1059198">Viber</option>
                                        <option value="1059199">Kakao Talk</option>
                                        <option value="1059200">None</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176825">SNS tool ID/URL/Mobile number</label></th>
                                <td><input type="text" name="questions[176825]" class="inputTextSingle" id="questions_176825" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176834">Company Skype ID</label></th>
                                <td><input type="text" name="questions[176834]" class="inputTextSingle" id="questions_176834" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176826">Website URL </label></th>
                                <td><input style="ime-mode: disabled;" type="text" name="questions[176826]" class="inputTextSingle" id="questions_176826" /></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176827">Business category <span class="komejirushi">(required)</span></label></th>
                                <td><ul class="checkbox_list"><li><input name="questions[176827][]" type="checkbox" value="1059201" id="questions_176827_1059201" />&nbsp;<label for="questions_176827_1059201">Trader</label></li>
                                        <li><input name="questions[176827][]" type="checkbox" value="1059202" id="questions_176827_1059202" />&nbsp;<label for="questions_176827_1059202">Manufacturer</label></li>
                                        <li><input name="questions[176827][]" type="checkbox" value="1059203" id="questions_176827_1059203" />&nbsp;<label for="questions_176827_1059203">Wholesaler</label></li>
                                        <li><input name="questions[176827][]" type="checkbox" value="1059204" id="questions_176827_1059204" />&nbsp;<label for="questions_176827_1059204">Retailer</label></li>
                                        <li><input name="questions[176827][]" type="checkbox" value="1059205" id="questions_176827_1059205" />&nbsp;<label for="questions_176827_1059205">E-commerce</label></li>
                                        <li><input name="questions[176827][]" type="checkbox" value="1059206" id="questions_176827_1059206" />&nbsp;<label for="questions_176827_1059206">Other</label></li></ul></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176828">Handled goods <span class="komejirushi">(required)</span></label></th>
                                <td><ul class="checkbox_list"><li><input name="questions[176828][]" type="checkbox" value="1059207" id="questions_176828_1059207" />&nbsp;<label for="questions_176828_1059207">Food</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059208" id="questions_176828_1059208" />&nbsp;<label for="questions_176828_1059208">Health &amp; Beauty</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059209" id="questions_176828_1059209" />&nbsp;<label for="questions_176828_1059209">Beverage</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059210" id="questions_176828_1059210" />&nbsp;<label for="questions_176828_1059210">Electronics</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059211" id="questions_176828_1059211" />&nbsp;<label for="questions_176828_1059211">Fashion</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059212" id="questions_176828_1059212" />&nbsp;<label for="questions_176828_1059212">Japanese Products</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059213" id="questions_176828_1059213" />&nbsp;<label for="questions_176828_1059213">Gift &amp; Novelty</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059214" id="questions_176828_1059214" />&nbsp;<label for="questions_176828_1059214">Second Hand</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059215" id="questions_176828_1059215" />&nbsp;<label for="questions_176828_1059215">Stationery</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059216" id="questions_176828_1059216" />&nbsp;<label for="questions_176828_1059216">Daily/Household</label></li>
                                        <li><input name="questions[176828][]" type="checkbox" value="1059217" id="questions_176828_1059217" />&nbsp;<label for="questions_176828_1059217">Industrial Goods</label></li></ul></td>
                            </tr>
                            <tr>
                                <th><label for="questions_176829">Desired product/s</label></th>
                                <td><textarea rows="8" cols="32" name="questions[176829]" id="questions_176829"></textarea></td>
                            </tr>
                        </table>
                        <input type="submit" value="Apply" class="submitBtn">

                    </form>
                </div>

            </section>
        </div>

    </div>
</div>
</body>
</html>