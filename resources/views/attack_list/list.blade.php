@extends('layouts.master')
@section('sekai_content')
    <div class="container-fluid">

        <div class="">
            <h1 class="h1 bg-primary text-center h1_list_user">アタックリスト</h1>


            <div class="row_detail_foreign">
                <table class="table table-hover table-bordered table-striped ">
                    <thead>
                    <tr class="bg-primary text-center">
                        <th class="text-center">レコード番号</th>
                        <th class="text-center">詳細</th>
                        <th class="text-center">ステータス</th>
                        <th class="text-center">依頼日</th>
                        <th class="text-center">提案納期</th>
                        <th class="text-center">国</th>
                        <th class="text-center">企業名</th>
                        <th class="text-center">調達依頼内容</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1111</td>
                        <td>
                            <a href="/company_foreign_detail" class="btn btn-warning po_a">詳細</a>
                        </td>
                        <td>
                            <select class="form-control" name="">
                                <option value="">提案済み</option>
                                <option value="">進展あり</option>
                                <option value="">進展なし</option>
                                <option value="">有償</option>
                                <option value="">無償</option>
                                <option value="">商談中</option>
                            </select>
                        </td>
                        <td>2016年12月31日</td>
                        <td>2017年1月1日</td>
                        <td>シンガポール</td>
                        <td>LUXASIA</td>
                        <td>希望、台湾</td>
                    </tr>
                    <tr>
                        <td>1112</td>
                        <td>
                            <a href="/company_foreign_detail" class="btn btn-warning po_a">詳細</a>
                        </td>
                        <td>
                            <select class="form-control" name="">
                                <option value="">提案済み</option>
                                <option value="">進展あり</option>
                                <option value="">進展なし</option>
                                <option value="">有償</option>
                                <option value="">無償</option>
                                <option value="">商談中</option>
                            </select>
                        </td>
                        <td>2016年12月31日</td>
                        <td>2017年1月1日</td>
                        <td>タイ</td>
                        <td>ISETAN BANGKOK</td>
                        <td>夢</td>
                    </tr>
                    <tr>
                        <td>1113</td>
                        <td>
                            <a href="/company_foreign_detail" class="btn btn-warning po_a">詳細</a>
                        </td>
                        <td>
                            <select class="form-control" name="">
                                <option value="">提案済み</option>
                                <option value="">進展あり</option>
                                <option value="">進展なし</option>
                                <option value="">有償</option>
                                <option value="">無償</option>
                                <option value="">商談中</option>
                            </select>
                        </td>
                        <td>2016年12月31日</td>
                        <td>2017年1月1日</td>
                        <td>香港</td>
                        <td>SOGO</td>
                        <td>明日</td>
                    </tr>
                    <tr>
                        <td>1114</td>
                        <td>
                            <a href="#" class="btn btn-warning po_a">詳細</a>
                        </td>
                        <td>
                            <select class="form-control" name="">
                                <option value="">提案済み</option>
                                <option value="">進展あり</option>
                                <option value="">進展なし</option>
                                <option value="">有償</option>
                                <option value="">無償</option>
                                <option value="">商談中</option>
                            </select>
                        </td>
                        <td>2016年12月31日</td>
                        <td>2017年1月1日</td>
                        <td>ベトナム</td>
                        <td>VIET DRAGON</td>
                        <td>未来</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection