@extends('layouts.master')
@section('title','HOME')
@section('sekai_content')
    <link rel="stylesheet" href="{{ url('/css/jquery.calendar/pickmeup.css') }}">

    <script src="{{ url('/js/jquery.calendar/pickmeup.js') }}"></script>
    <script type="text/javascript">
//        google.charts.load('current', {'packages':['corechart']});
        function drawVisualization(dt, container) {
            // Some raw data (not necessarily accurate)
            var data = google.visualization.arrayToDataTable(dt);

            var options = '';

            var options = {
                legend: { position: 'none' },
                seriesType: 'bars',
                series: {1: {type: 'line'}},
                colors: ['#dca752','#2cb18d'],
                pointSize: 10,
                'width':1314,
                'height':532,
                backgroundColor: { fill:'transparent'},
                bar: { groupWidth: '40' },
                isStacked: true,
                chartArea:{left:0,top:0,width:"100%",height:"96%"},
                //hAxis: { textPosition: 'none' },
                vAxis : { textPosition: 'none',gridlines: { color : 'transparent'}},
            };

            if(container == 'chart_div_4'){
                options = {
                    legend: { position: 'none' },
                    seriesType: 'bars',
                    series: {1: {type: 'line'}},
                    colors: ['#dca752','#2cb18d'],
                    pointSize: 10,
                    'width':1314,
                    'height':532,
                    backgroundColor: { fill:'transparent'},
                    bar: { groupWidth: '40' },
                    isStacked: true,
                    chartArea:{left:0,top:10,width:"100%",height:"100%"},
                    hAxis: { textPosition: 'none' },
                    vAxis : { textPosition: 'none',gridlines: { color : 'transparent'}},
                };
            }else{
                options = {
                    legend: { position: 'none' },
                    seriesType: 'bars',
                    series: {1: {type: 'line'}},
                    colors: ['#dca752','#2cb18d'],
                    pointSize: 10,
                    'width':1314,
                    'height':532,
                    backgroundColor: { fill:'transparent'},
                    bar: { groupWidth: '40' },
                    isStacked: true,
                    chartArea:{left:0,top:10,width:"100%",height:"94%"},
                    //hAxis: { textPosition: 'none' },
                    vAxis : { textPosition: 'none',gridlines: { color : 'transparent'}},
                };
            }

            var chart = new google.visualization.ComboChart(document.getElementById(container));
            chart.draw(data, options);
        }
    </script>
    <style>
        .td-border{
            border: 2px solid #ccc;
        }
        .td-number{
            width: 92px;
            vertical-align: middle;
        }
        .td-bold{
            font-weight: bold;
        }

        .form-group label {
            float: left;
            text-align: left;
            font-weight: normal;
        }

        .form-group select {
            display: inline-block;
            width: auto;
            vertical-align: middle;
        }
        .line {
            display:inline-block;
            border: 1px solid #ccc;
            /*margin: 10px;*/
            /*padding: 10px;*/
            background:url('http://i.piccy.info/i7/c7a432fe0beb98a3a66f5b423b430423/1-5-1789/1066503/lol.png');
            background-size:100% 100%;
        }

        .fix-header-table {
            padding: 0 !important;
            overflow: hidden;
            border: 1px solid #ccc !important;
        }

        .fix-header-table span:first-child {
            position: absolute;
            bottom: 5px;
            left: 5px;
        }

        .fix-header-table span:last-child {
            position: absolute;
            top: 5px;
            right: 5px;
        }
        .fix-header-table::after {
            content: "";
            display: block;
            position: absolute;
            border-bottom: 1px solid #ccc;
            width: 200%;
            transform: rotate(14deg);
            transform-origin: top left;

        }

    </style>

    <section class="fixed-content">
        <header><h2>ホーム</h2></header>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab">統計情報</a></li>

            <?php
//                echo '<li role="presentation"><a href="#activity-transition" aria-controls="activity-transition" role="tab" data-toggle="tab">活動偏差値</a></li>';
                    ?>
        </ul>
        <article>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="statistics">
                    @include('home.home_2')

                    @include('home.home_3')
                </div>
                <!-- end .tab-pane -->
                <div role="tabpanel" class="tab-pane" id="activity-transition">
                    @include('home.home_4')

                    @include('home.home_5')
                </div>
            </div>
        </article>
    </section>


    <script>
        $(document).ready(function() {
            $('.nav-tabs a:first').tab('show');
        });
        $(window).scroll(function() {
            if ($(this).scrollTop() >= 550) {
                $('#return-to-top').fadeIn(200);
            } else {
                $('#return-to-top').fadeOut(200);
            }
        });
        $('#return-to-top').click(function() {
            $('body,html').animate({
                scrollTop : 0
            }, 500);
        });
    </script>

@endsection