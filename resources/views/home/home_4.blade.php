<meta name="_token" content="{{ csrf_token() }}">
<div class="content-charts-01">
    <div class="title-chart-01 clearfix">{{\Illuminate\Support\Facades\Auth::user()->supplier->company_name_jp}}様の活動偏差値
        <div class="pull-right">
            <select id="category_4" class="mgR-37">
                <option value="0">全体偏差値</option>
                @foreach($categories as $categorie)
                    <option value="{{ $categorie->id}}">{{ $categorie->content}}</option>
                @endforeach
            </select>
            <span class="mgR-16">期間選択</span>
            <input type="text" name="graph_3_start_4" class="graph_3_start_4" id="graph_3_start_4" value="{{date("Y/m/d", strtotime("-4 month", strtotime(date("Y/m/d"))))}}"><span class="mgLR-20">-</span><input type="text" class="graph_3_end_4" name="graph_3_end_4" id="graph_3_end_4" value="{{date("Y/m/d")}}">
            <button type="button" class="btn-register_4">適用</button>
        </div>
    </div>

    <div id="chart_div_4" class="content-chart-01"></div>

    <div id="tips_bottom" class="content-tips-01 clearfix">
        <div class="tips">
            <div class="tip">入札<span>{{ $arr_graph_tab2['status_bid'] }}</span></div>
        </div>
        <!-- end .tips -->
        <div class="tips">
            <div class="tip">提案<span>{{ $arr_graph_tab2['status_suggestsent'] }}</span></div>
        </div>
        <!-- end .tips -->
        <div class="tips">
            <div class="tip">進展あり返信<span>{{ $arr_graph_tab2['status_proccess'] }}</span></div>
        </div>
        <div class="tips">
            <div class="tip">有償サンプル<span>{{ $arr_graph_tab2['status_sample_charge'] }}</span></div>
        </div>
        <!-- end .tips -->
        <div class="tips">
            <div class="tip">受注<span>{{ $arr_graph_tab2['status_order'] }}</span></div>
        </div>
        <!-- end .tips -->
        <div class="tips">
            <div class="tip">リピート<span>{{ $arr_graph_tab2['status_repeat'] }}</span></div>
        </div>
        <!-- end .tips -->
    </div>

</div>

    <div class="content-table-01">
        <table id="devition_tb">
            <thead>
            <tr>
                <th>活動偏差値</th>
                <th>入札</th>
                <th>提案</th>
                <th>進展あり返信</th>
                <th>有償サンプル</th>
                <th>受注</th>
                <th>リピート</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            </tr>
            </tbody>
        </table>
    </div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#period_4').change(function () {
            number_period = $('#period_4').val();
            pickmeup('.sekai-calendar_4').destroy();
            pickmeup('.sekai-calendar_4', {
                flat      : true,
                date      : [
                    new Date()
                ],
                class_name: 'calendar_4',
                mode      : 'range',
                calendars : number_period,
                format    : 'Y/m/d',
            });
            pickmeup(".sekai-calendar_4").set_date([$(".graph_3_start_4").val(), $(".graph_3_end_4").val()]);
        });
        $(function () {
            number_period = 3;
            pickmeup('.sekai-calendar_4', {
                flat      : true,
                date      : [
                    new Date()
                ],
                class_name: 'calendar_4',
                mode      : 'range',
                calendars : 3,
                format    : 'Y/m/d',
            });

            pickmeup('.graph_3_start_4', {
                position       : 'bottom',
                hide_on_select : true,
                format    : 'Y/m/d',
                date : $("#graph_3_start_4").val(),
            });

            pickmeup('.graph_3_end_4', {
                position       : 'bottom',
                hide_on_select : true,
                format    : 'Y/m/d',
                date : $("#graph_3_end_4").val(),
            });
            var temp_start_date = $(".graph_3_start_4").val();
            var temp_end_date = $(".graph_3_end_4").val();

            $(".sekai-calendar_4").on('pickmeup-change', function (e) {
                $(".graph_3_start_4").val(e.detail.formatted_date[0]);
                $(".graph_3_end_4").val(e.detail.formatted_date[1]);
                temp_start_date = e.detail.formatted_date[0];
                temp_end_date = e.detail.formatted_date[1];
            });

            $(".graph_3_end_4, .graph_3_start_4").on('pickmeup-change', function (e) {
                var startDate = $(".graph_3_start_4").val();
                var endDate = $(".graph_3_end_4").val();

                var startDateGraph = new Date(startDate);
                var endDateGraph = new Date(endDate);

                var diffMonth = monthDiff(startDateGraph, endDateGraph);
                if(diffMonth >= 12){
                    alert("12ヶ月以内で統計してください。");
                    $(".graph_3_start_4").val(temp_start_date);
                    $(".graph_3_end_4").val(temp_end_date);
                    return;
                }

                if(endDateGraph.getTime() <  startDateGraph.getTime()){
                    alert("開始日は終了日前にする必要があります。");
                    $(".graph_3_start_4").val(temp_start_date);
                    $(".graph_3_end_4").val(temp_end_date);
                    return;
                }
                pickmeup(".sekai-calendar_4").set_date([startDate, endDate]);
                temp_start_date = startDate;
                temp_end_date = endDate;
            });
        });
        loadGrapth();
        $(".btn-register_4").click(function(e) {
            loadGrapth();
        });

        function loadGrapth() {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            var categoryId = $('#category_4').val();
            var startDate = $(".graph_3_start_4").val();
            var endDate = $(".graph_3_end_4").val();

            var current_token = '{{csrf_token()}}';
            var dt = [['Month', '件', { role: 'tooltip' }, '件', { role: 'tooltip' }]];
            $.ajax({
                url: '/home_4.html',
                dataType: 'json',
                type: 'post',
                contentType: 'application/x-www-form-urlencoded',
                data: {categoryId:categoryId, startDate:startDate, endDate:endDate, fuel_csrf_token: current_token},
                success: function( data, textStatus, jQxhr ){
                    var total = data.total;
                    var column = data.deviation;
                    var array = [];
                    array = ['入札',total['sup_bid'],total['bid']];
                    dt.push(['入札',total['sup_bid'], '入札\n' + total['sup_bid'],total['bid'], '入札\n' + total['bid'].toFixed(2)],
                            ['提案',total['sup_sugesstion'], '提案\n' + total['sup_sugesstion'],total['sugesstion'], '提案\n' + total['sugesstion'].toFixed(2)],
                            ['進展あり返信', total['sup_reply'], '進展あり返信\n' + total['sup_reply'], total['reply'], '進展あり返信\n' + total['reply'].toFixed(2)],
                            ['サンプル',  total['sup_sample'], 'サンプル\n' + total['sup_sample'], total['sample'], 'サンプル\n' + total['sup_sample'].toFixed(2)],
                            ['受注',  total['sup_order'], '受注\n' + total['sup_order'], total['order'], '受注\n' + total['order'].toFixed(2)],
                            ['リピート',  total['sup_repeat'], 'リピート\n' + total['sup_repeat'], total['repeat'], 'リピート\n' + total['repeat'].toFixed(2)]);

//                google.charts.setOnLoadCallback(function(){
//                    drawVisualization(dt, "chart_div_4");
//                });
                    $("#tips_bottom").html('<div class="tips"> <div class="tip">入札<span>'+ total['sup_bid'] +'</span></div> </div>'
                             + '<div class="tips"> <div class="tip">提案<span>'+ total['sup_sugesstion'] + '</span></div>'
                            + '</div> <div class="tips"> <div class="tip">進展あり返信<span>' + total['sup_reply'] + '</span></div>'
                            + '</div> <div class="tips"> <div class="tip">有償サンプル<span>'+ total['sup_sample'] +'</span></div>'
                            + '</div> <div class="tips"> <div class="tip">受注<span>' + total['sup_order'] + '</span></div>'
                            + '</div> <div class="tips"> <div class="tip">リピート<span>' + total['sup_repeat'] + '</span></div> </div>');

                    google.charts.setOnLoadCallback(function(){
                        drawVisualization(dt, "chart_div_4");
                    });
                    $("#devition_tb > tbody > tr").html('<td>貴社偏差値</td>');
                    $("#devition_tb > tbody > tr").append('<td >'+column['bid'].toFixed(2)+'</td><td >'+column['sugesstion'].toFixed(2)+'</td><td >'+column['reply'].toFixed(2)+'</td><td >'
                            +column['sample'].toFixed(2)+'</td><td >'+column['order'].toFixed(2)+'</td><td >'+column['repeat'].toFixed(2)+'</td>'
                    );

                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });
        }
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
            return months <= 0 ? 0 : months;
        }
    });

</script>

<script type="text/javascript">
//    var dt = [
//        ['Month', '1', '2'],
//        ['11 月 2016',  2, 0.18],
//        ['12 月 2016',  3, 0.27],
//        ['1 月 2017',  1, 0.09],
//        ['2 月 2017',  2, 0.18],
//        ['3 月 2017',  2, 0.18],
//        ['4 月 2017',  1, 0.09]
//    ];

//    google.charts.setOnLoadCallback(function(){
//        drawVisualization(dt, "chart_div_4");
//    });
</script>