<div class="content-charts-01">

    <div class="title-chart-01 clearfix">{{\Illuminate\Support\Facades\Auth::user()->supplier->company_name_jp}}様の活動偏差値 推移
        <div class="pull-right">
            <span class="mgR-16">期間選択</span>
            <input type="text" name="graph_3_start_5" id="graph_3_start_5" class="graph_3_start_5" value="{{date("Y/m/d", strtotime("-4 month", strtotime(date("Y/m/d"))))}}"><span class="mgLR-20">-</span><input class="graph_3_end_5" type="text" name="graph_3_end_5" id="graph_3_end_5" value="{{date("Y/m/d")}}">
            <button type="button" id="graph_5_click">適用</button>
        </div>
    </div>

    <!-- end .title-chart-01 --> 
    <div class="content-group-button" id="attack_status_graph_fix_02">
             <button type="button" class="active" data-value="{{config('constants.STATUS_BID' )}}">入札</button>
             <button type="button" data-value="{{config('constants.STATUS_SUGGESTSENT' )}}">提案</button>
             <button type="button" data-value="{{config('constants.STATUS_PROCCESS')}}">進展あり返信</button> 
        <button type="button" data-value="{{config('constants.STATUS_SAMPLE_FREE').",".config('constants.STATUS_SAMPLE_CHARGE')}}">サンプル</button>
             <button type="button" data-value="{{config('constants.STATUS_ORDER')}}">受注</button> 
        <button type="button" data-value="{{config('constants.STATUS_REPEAT')}}">リピート</button> 
    </div> 
    <!-- end .content-group-button -->

    <div id="chart_div_5" class="content-chart-01"></div>

</div>
<div class="content-table-01 fix-content-table-01 clearfix">
    <table class="pull-left">
        <thead>
        <tr><th>営業偏差値</th></tr>
        </thead>
        <tbody>
        <tr><td>貴社偏差値</td></tr>
        </tbody>
    </table>
    <div class="content-overflow-01 pull-left">
        <table id="grap_5">
            <thead>
                <tr></tr>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>
    </div>
    <!-- end .content-overflow-01 -->
</div>
<!-- end .content-table-01 -->


<script type="text/javascript">
    $(document).ready(function () {
        show_graph();
        $("#graph_5_click").click(show_graph);
        function show_graph() {
            var CSRF_TOKEN = '{{ csrf_token() }}';
            var attack_status = $('#attack_status_graph_fix_02 .active').attr('data-value');
            var dt = {
                '_token': CSRF_TOKEN,
                'status'    : attack_status,
                'from_date'   : $("#graph_3_start_5").val(),
                'to_date' : $("#graph_3_end_5").val()
            };
            $.ajax({
                type: "POST",
                url: "/graph/get_data_graph_4",
                data: dt,
                dataType: "json",
                success: function (data) {
                    var chart_data = [['Month', '件', '件']];
                    var array = [];
                    var out_put_tr_1 = '';
                    var out_put_tr_2 = '';
                    $.each(data , function (key , value) {
                        array = [value['date_month'], value['company_current'], value['averagre_supplier']];
                        chart_data.push(array);
                        out_put_tr_1 += '<th>'+value['date_month']+'</th>';
                        out_put_tr_2 += '<td>'+value['deviation'].toFixed(2)+'</td>';
                    });
                    $("#grap_5 > thead > tr").html(out_put_tr_1);
                    $("#grap_5 > tbody > tr").html(out_put_tr_2);
                    google.charts.setOnLoadCallback(function(){
                        drawVisualization(chart_data, "chart_div_5");
                    });
                }
            });
        }
        $('#period_5').change(function () {
            number_period = $('#period_5').val();
            pickmeup('.sekai-calendar_5').destroy();
            pickmeup('.sekai-calendar_5', {
                flat      : true,
                date      : [
                    new Date()
                ],
                class_name: 'calendar_5',
                mode      : 'range',
                calendars : number_period,
                format    : 'Y/m/d',
            });
            pickmeup(".sekai-calendar_5").set_date([$(".graph_3_start_5").val(), $(".graph_3_end_5").val()]);
        });
        $(function () {
            number_period = 3;
            pickmeup('.sekai-calendar_5', {
                flat      : true,
                date      : [
                    new Date()
                ],
                class_name: 'calendar_5',
                mode      : 'range',
                calendars : 3,
                format    : 'Y/m/d',
            });

            pickmeup('.graph_3_start_5', {
                position       : 'bottom',
                hide_on_select : true,
                format    : 'Y/m/d',
                date: $("#graph_3_start_5").val()
            });

            pickmeup('.graph_3_end_5', {
                position       : 'bottom',
                hide_on_select : true,
                format    : 'Y/m/d',
                date: $("#graph_3_end_5").val()
            });
            var temp_start_date = $(".graph_3_start_5").val();
            var temp_end_date = $(".graph_3_end_5").val();

            $(".sekai-calendar_5").on('pickmeup-change', function (e) {
                $(".graph_3_start_5").val(e.detail.formatted_date[0]);
                $(".graph_3_end_5").val(e.detail.formatted_date[1]);
                temp_start_date = e.detail.formatted_date[0];
                temp_end_date = e.detail.formatted_date[1];
            });

            $(".graph_3_end_5, .graph_3_start_5").on('pickmeup-change', function (e) {
                var startDate = $(".graph_3_start_5").val();
                var endDate = $(".graph_3_end_5").val();

                var startDateGraph = new Date(startDate);
                var endDateGraph = new Date(endDate);
                var diffMonth = monthDiff(startDateGraph, endDateGraph);
                if(diffMonth >= 12){
                    alert("12ヶ月以内で統計してください。");
                    $(".graph_3_start_5").val(temp_start_date);
                    $(".graph_3_end_5").val(temp_end_date);
                    return;
                }
                if(endDateGraph.getTime() <  startDateGraph.getTime()){
                    alert("開始日は終了日前にする必要があります。");
                    $(".graph_3_start_5").val(temp_start_date);
                    $(".graph_3_end_5").val(temp_end_date);
                    return;
                }
                pickmeup(".sekai-calendar_5").set_date([startDate, endDate]);
                temp_start_date = startDate;
                temp_end_date = endDate;
            });
        });
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
            return months <= 0 ? 0 : months;
        }
    });

</script>
