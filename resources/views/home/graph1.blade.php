@extends('layouts.master')
@section('title','HOME')
@section('sekai_content')

    <style>
        .td-border{
            border: 1px solid #ccc;
        }
        .td-number{
            width: 92px;
            vertical-align: middle;
        }
    </style>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tabs1">Tabs 1</a></li>
            <li><a data-toggle="tab" href="#tabs2">Tabs 2</a></li>
        </ul>

        <div class="tab-content">
            <div id="tabs1" class="tab-pane fade in active">
                <script type="text/javascript">
                    google.charts.load("current", {packages:['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Genre', 'Fantasy & Sci Fi', 'Romance',  { role: 'annotation' } ],
                            ['入札', 45, null, ''],
                            ['提案', 40, null, ''],
                            ['返信', 15, 5, ''],
                            ['サンプル', 10, 5,  ''],
                            ['受注', 3, null,  ''],
                            ['リピート', 2, null,  '']
                        ]);

                        var view = new google.visualization.DataView(data);
                        view.setColumns([0, 1,
                            { calc: "stringify",
                                sourceColumn: 3,
                                type: "string",
                                role: "annotation" },
                            2]);

                        var options = {
                            legend: { position: 'none' },
                            bar: { groupWidth: '75%' },
                            isStacked: true,
                            colors: ['#BFEFFF','#EE6363'],
                        };

                        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                        chart.draw(view, options);
                    }
                </script>
                <div class="container-fluid">
                    <div class="text-center">
                        <div id="columnchart_values" style="width: 1500px; height: 500px;margin-top: 10px;margin-bottom: 10px;"></div>
                    </div>
                </div>
                <table class="text-center" style="width:1120px;margin-top: 10px;margin-bottom: 10px;margin-left: 210px;">
                    <tr>
                        <td class="td-border td-number">件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">進展なし<br>件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">無償サンプル<br>件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">件数</td>
                        <td class="td-border td-number">0</td>
                    </tr>
                    <tr>
                        <td class=""></td>
                        <td class=""></td>
                        <td class="td-border td-number">提案率</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">進展なし<br>発展率</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">無償サンプル<br>発展率</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">発展率</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">発展率</td>
                        <td class="td-border td-number">0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="td-border td-number">進展あり<br>件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">有償サンプル<br>件数</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">金額 / JPY</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">金額 / JPY</td>
                        <td class="td-border td-number">0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="td-border td-number">進展あり<br>発展率</td>
                        <td class="td-border td-number">0</td>
                        <td class="td-border td-number">有償サンプル<br>発展率</td>
                        <td class="td-border td-number">0</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div id="tabs2" class="tab-pane fade" style="width: 1500px; height: 500px">
                <script type="text/javascript">
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawVisualization);


                    function drawVisualization() {
                        // Some raw data (not necessarily accurate)
                        var data = google.visualization.arrayToDataTable([
                            ['Month', '1', '2'],
                            ['1 月',  500, 520],
                            ['2 月',  450, 350],
                            ['3 月',  500, 380],
                            ['4 月',  440, 350],
                            ['5 月',  420, 375],
                            ['6 月',  480, 300]

                        ]);

                        var options = {
                            legend: { position: 'none' },
                            seriesType: 'bars',
                            series: {1: {type: 'line'}},
                            colors: ['#BFEFFF','#EE6363'],
                            pointSize: 10,
                            'width':1500,
                            'height':500
                        };

                        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
                        chart.draw(data, options);
                    }
                </script>

                <div class="container-fluid">
                    <div class="text-center">
                        <div id="chart_div" style="margin-top: 10px;margin-bottom: 10px;"></div>
                    </div>
                </div>
                <table class="table text-center table-bordered" style="width:1260px;margin-top: 10px;margin-bottom: 10px;margin-left: 60px">
                    <tr>
                        <td align="left" width="150">貴社</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td align="left" width="150">発展率</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </table>
            </div>
        </div>


@endsection