<?php

//if (! empty($greeting)) {
//    echo $greeting, "\n\n";
//} else {
//    echo $level == 'error' ? 'Whoops!' : 'Hello!', "\n\n";
//}
//
//if (! empty($introLines)) {
//    echo implode("\n", $introLines), "\n\n";
//}
//
//if (isset($actionText)) {
//    echo "{$actionText}: {$actionUrl}", "\n\n";
//}
//
//if (! empty($outroLines)) {
//    echo implode("\n", $outroLines), "\n\n";
//}
//
//echo 'Regards,', "\n";
//echo config('app.name'), "\n";

echo "セカイコネクトサポート窓口です","\n";
echo "アカウント パスワードを リセットするには、次の","\n";
echo "リンクをクリックしてください。","\n";

echo "<a href='$actionUrl' class='button' target='_blank'>$actionUrl</a>","\n";

echo "このリンクをクリックしても機能しない場合は、URL をコピーして新しいブラウザ","\n";
echo "ウィンドウに貼り付けてください。","\n";

echo "このメールに心当たりがない場合、他の方がパスワードをリセットする際に誤ってお","\n";
echo "客様のメール アドレスを入力した可能性があります。リクエストした覚えがない場","\n";
echo "合は、何も行わずにこのメールを破棄してください。","\n";

echo "セカイコネクトをご利用いただきありがとうございます。","\n";

echo "本メールは配信専用です。ご返信なさらぬようご注意ください。","\n";
echo "―――――――――――――――――――――――■―――――――――――――――――■■□","\n";
echo "COUXU株式会社","\n";
echo "サポート窓口","\n";
echo "■■■〒101-0042 東京都千代田区神田東松下町31-1 神田三義ビル4F","\n";
echo "■■■Tel：03-5298-5190","\n";
echo "■■■Fax：03-3525-8972","\n";
echo "■■■Mail：couxu@commerce-sourcing.com","\n";
echo "■■■HP：http://couxu.jp","\n";
echo "■■■Media：http://world-conect.com/","\n";
echo "――――――――■――――――――――――――――――――――――――――――――■□■","\n";


